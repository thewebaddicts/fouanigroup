<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class lebanoncontactus extends Controller
{
    public function contactuslebanon(Request $request)
    {
     
        $slideshow=\App\Models\Lebanon::where('cancelled',0)->get();
        $overview=\App\Models\Lebanonoverview::where('cancelled',0)->get();
        $item=\App\Models\Lebanonrealstate::where('cancelled',0)->get();
        $info_form = new \App\Models\Lebanoncontactus();
        $info_form->firstname = $request->firstname;
        $info_form->lastname = $request->lastname;
        $info_form->emailaddress = $request->emailaddress;

      
        $info_form->country = $request->country;
        $info_form->message = $request->message;

        $info_form->save();
        return view('pages.lebanon',['slideshow'=>$slideshow,'overview'=>$overview,'item'=>$item]);


      
      
    }
    
}
