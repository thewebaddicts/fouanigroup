<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class congocontactuscontroller extends Controller
{
    public function contactuscongo(Request $request)
    {
        $slideshow=\App\Models\congohomepage::where('cancelled',0)->get();
        $over=\App\Models\congooverview::where('cancelled',0)->get();
      
       

        $info_form = new \App\Models\congocontactus();
        $info_form->firstname = $request->firstname;
        $info_form->lastname = $request->lastname;
        $info_form->emailaddress = $request->emailaddress;

      
        $info_form->country = $request->country;
        $info_form->message = $request->message;

        $info_form->save();
        return view('pages.congohome',['slideshow'=>$slideshow,'over'=>$over]); 

      
      
    }
}
