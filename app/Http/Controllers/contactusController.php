<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class contactusController extends Controller
{
    public function contactus(Request $request)
    {
        $slideshow=\App\Models\homepagemodel::where('cancelled',0)->get();
        $aboutus=\App\Models\hompepageaboutusmodel::where('cancelled',0)->get();
        $mainbrands=\App\Models\homepagemainbrandsmodel::where('cancelled',0)->get();
        $country=\App\Models\homepagecountriesmodel::where('cancelled',0)->get();
       

        $info_form = new \App\Models\homepagecontactus();
        $info_form->firstname = $request->firstname;
        $info_form->lastname = $request->lastname;
        $info_form->email = $request->email;

      
        $info_form->country = $request->country;
        $info_form->message = $request->message;

        $info_form->save();
        return view('pages.home',['slideshow'=>$slideshow,'aboutus'=>$aboutus,'mainbrands'=>$mainbrands,'country'=>$country]); 

      
      
    }
}
