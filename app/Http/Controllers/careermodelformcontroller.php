<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class careermodelformcontroller extends Controller
{
    public function careerpage(Request $request)
    {
        $careers=\App\Models\careermodel::where('cancelled',0)->get();

     
      

        $info_form = new \App\Models\careermodelform();
        $info_form->firstname = $request->firstname;
        $info_form->lastname = $request->lastname;
        $info_form->emailaddress = $request->emailaddress;

      
        $info_form->country = $request->country;
        $info_form->cv = $request->cv;

        $info_form->message = $request->message;

        $info_form->save();
        return view('pages.careers',['careers'=>$careers]);



      
      
    }
}
