<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class contactusnigeriaController extends Controller
{
    public function contactusnigeria(Request $request)
    {
        $slideshow=\App\Models\nigeriahomepage::where('cancelled',0)->get();
        $overview=\App\Models\nigeriaoverview::where('cancelled',0)->get();
        $brands=\App\Models\nigeribrands::where('cancelled',0)->get();
        $outlits=\App\Models\nigerioutlits::where('cancelled',0)->get();
        $fmcg=\App\Models\fmcg::where('cancelled',0)->get();
        $factory=\App\Models\nigeriafactory::where('cancelled',0)->get();
       

        $info_form = new \App\Models\nigeriacontactus();
        $info_form->firstname = $request->firstname;
        $info_form->lastname = $request->lastname;
        $info_form->emailaddress = $request->emailaddress;

      
        $info_form->country = $request->country;
        $info_form->message = $request->message;

        $info_form->save();
        return view('pages.Nigeriahome',['slideshow'=>$slideshow,'overview'=>$overview,'brands'=>$brands,'outlits'=>$outlits,'fmcg'=>$fmcg,'factory'=>$factory]);

      
      
    }
}
