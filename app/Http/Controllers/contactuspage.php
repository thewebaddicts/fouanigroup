<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class contactuspage extends Controller
{
    public function contactuspage(Request $request)
    {
     
        $contactus=\App\Models\contactus::where('cancelled',0)->get();

        $info_form = new \App\Models\contactusform();
        $info_form->firstname = $request->firstname;
        $info_form->lastname = $request->lastname;
        $info_form->email = $request->email;

      
        $info_form->country = $request->country;
        $info_form->message = $request->message;

        $info_form->save();
        return view('pages.contactus',['contactus'=>$contactus]);


      
      
    }
}
