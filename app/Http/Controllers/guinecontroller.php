<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class guinecontroller extends Controller
{
    public function contactusguinea(Request $request)
    {
     
        $slideshow=\App\Models\guineahomepage::where('cancelled',0)->get();
        $overview=\App\Models\guineaoverview::where('cancelled',0)->get();
        $fmcg=\App\Models\fmcg::where('cancelled',0)->get();
        $brand=\App\Models\guineelectricbrand::where('cancelled',0)->get();
        $factory=\App\Models\guinefactory::where('cancelled',0)->get();
    

        $info_form = new \App\Models\guinecontactus();
        $info_form->firstname = $request->firstname;
        $info_form->lastname = $request->lastname;
        $info_form->emailaddress = $request->emailaddress;

      
        $info_form->country = $request->country;
        $info_form->message = $request->message;

        $info_form->save();
        return view('pages.guinea',['slideshow'=>$slideshow,'overview'=>$overview,'fmcg'=>$fmcg,'brand'=>$brand,'factory'=>$factory]);


      
      
    }
}
