<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class gambiacontroller extends Controller
{
    public function contactusgambia(Request $request)
    {
     
        $slideshow=\App\Models\gambiahome::where('cancelled',0)->get();
        $overview=\App\Models\gambiaoverview::where('cancelled',0)->get();
        $brand=\App\Models\gambiaelectricbrands::where('cancelled',0)->get();
        $fmcg=\App\Models\fmcg::where('cancelled',0)->get();
    
    

        $info_form = new \App\Models\gambiacontactus();
        $info_form->firstname = $request->firstname;
        $info_form->lastname = $request->lastname;
        $info_form->emailaddress = $request->emailaddress;

      
        $info_form->country = $request->country;
        $info_form->message = $request->message;

        $info_form->save();
        return view('pages.gambia',['slideshow'=>$slideshow,'overview'=>$overview,'brand'=>$brand,'fmcg'=>$fmcg]);


      
      
    }
    
}
