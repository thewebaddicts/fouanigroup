<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class liberiacontactus extends Controller
{
    public function contactusliberia(Request $request)
    {
        $slideshow=\App\Models\liberiahome::where('cancelled',0)->get();
        $overview=\App\Models\liberiaoverview::where('cancelled',0)->get();
        $brands=\App\Models\liberiaelectricbrand::where('cancelled',0)->get();
        $fmcg=\App\Models\fmcg::where('cancelled',0)->get();
        $item=\App\Models\liberiapackgingtitle::where('cancelled',0)->get();
     
       

        $info_form = new \App\Models\liberiacontactus();
        $info_form->firstname = $request->firstname;
        $info_form->lastname = $request->lastname;
        $info_form->emailaddress = $request->emailaddress;

      
        $info_form->country = $request->country;
        $info_form->message = $request->message;

        $info_form->save();
        return view('pages.liberiahome',['slideshow'=>$slideshow,'overview'=>$overview,'brands'=>$brands,'fmcg'=>$fmcg,'item'=>$item]);

      
      
    }
}
