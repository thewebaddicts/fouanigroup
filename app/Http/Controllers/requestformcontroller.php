<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EmailTemplateModel;
use Illuminate\Support\Facades\Mail;
use App\Mail\emailtemplate;

class requestformcontroller extends Controller
{
    public function requestpage(Request $request)
    {

        $categories=\App\Models\categoriesandbrandmodel::where('cancelled',0)->get();
        $brands=\App\Models\electronicsbrandsmodel::where('cancelled',0)->get();
    
      

        $info_form = new \App\Models\requestform();
        $info_form->brand = $request->brand;
        $info_form->itemcategory = $request->itemcategory;
        $info_form->itemmodel = $request->itemmodel;
        



        $info_form->firstname = $request->firstname;
        $info_form->lastname = $request->lastname;
        $info_form->emailaddress = $request->emailaddress;
        $info_form->phone = $request->phone;
        $info_form->region = $request->region;
        $info_form->city = $request->city;
        $info_form->street = $request->street;
        $info_form->apartment = $request->apartment;
        $info_form->date = $request->date;
        $info_form->time = $request->time;


        $info_form->save();
        $template = EmailTemplateModel::where('cancelled', 0)->where('location', 'request_newsletter')->first();
        if ($template) {
            $dictionary = json_decode(json_encode([
                "form_data" => [
                    "Firs Name" => $request->input('firstname'),
                    "Last Name" => $request->input('lastname'),
                    "Email" => $request->input('emailaddress'),
                    "Phone" => $request->input('phone'),
                    "region" => $request->input('region'),
                    "city" => $request->input('city'),
                    "street" => $request->input('street'),
                    "apartment" => $request->input('apartment'),
                    "date" => $request->input('date'),
                    "time" => $request->input('time'),
                  
                ]
            ]));
           


            $company_email = explode(',', $template->email);
            
            foreach ($company_email as $email) {
                Mail::to(request()->input('email'))->queue(new emailtemplate("request_newsletter_client", $dictionary));
                
                Mail::to($email)->send(new emailtemplate('request_newsletter', $dictionary));
                
            }
        }

        return redirect()->route('contact', ["notification_id" => 1]);



      
      
    }
}
