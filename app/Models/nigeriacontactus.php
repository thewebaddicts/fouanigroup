<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class nigeriacontactus extends Model
{
    protected $table='contactusnigeria';
    use HasFactory;
}
