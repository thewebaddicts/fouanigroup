<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class gambiafmcg extends Model
{
    protected $table='gambiafmcg';
    use HasFactory;
}
