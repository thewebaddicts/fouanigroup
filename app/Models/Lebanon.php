<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lebanon extends Model
{
    protected $table='lebanonhompage';
    use HasFactory;
}
