<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class liberiaelectricbrand extends Model
{
    protected $table='liberiaelectronicbrands';
    use HasFactory;
}
