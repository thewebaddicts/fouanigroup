<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class contactusform extends Model
{
    protected $table='contactusform';
    use HasFactory;
}
