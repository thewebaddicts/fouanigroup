<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class errortypemodel extends Model
{
    protected $table='errortypepage';
    use HasFactory;
}
