<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class gambiacontactus extends Model
{
    protected $table='gambiacontactus';
    use HasFactory;
}
