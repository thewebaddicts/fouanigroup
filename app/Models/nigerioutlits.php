<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class nigerioutlits extends Model
{
    protected $table='nigeriaoutlets';
    use HasFactory;
    public function status()
    {
        return $this->belongsTo(\App\Models\map::class,'outletname_2');
    }
}

