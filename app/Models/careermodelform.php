<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class careermodelform extends Model
{
    protected $table='careerform';
    use HasFactory;
}
