<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class congooverview extends Model
{
    protected $table='congooverview';
    use HasFactory;
}
