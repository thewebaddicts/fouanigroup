<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lebanonoverview extends Model
{
    protected $table='lebanonoverview';
    use HasFactory;
}
