<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class electronicsbrandsmodel extends Model
{
    protected $table='electricbrandscategories';
    use HasFactory;
}
