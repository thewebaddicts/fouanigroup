<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class nigeriahomepage extends Model
{
    protected $table='nigeriaslideshow';
    use HasFactory;
}
