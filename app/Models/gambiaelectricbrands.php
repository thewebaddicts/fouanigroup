<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class gambiaelectricbrands extends Model
{
    protected $table='gambiaelectronicbrands';
    use HasFactory;
}
