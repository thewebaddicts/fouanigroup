<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class itemsmodel extends Model
{
    protected $table='itemdetails';
    use HasFactory;
}
