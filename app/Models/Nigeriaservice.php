<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nigeriaservice extends Model
{
    protected $table='nigeriaservice';
    use HasFactory;
}
