<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class fmcgslideshowmodel extends Model
{
    protected $table='liberiahomepage';
    use HasFactory;
}
