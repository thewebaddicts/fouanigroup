<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class congohomepage extends Model
{
    protected $table='congohomepage';
    use HasFactory;
}
