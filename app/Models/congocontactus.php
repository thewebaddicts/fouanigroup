<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class congocontactus extends Model
{
    protected $table ='contactuscongo';
    use HasFactory;
}
