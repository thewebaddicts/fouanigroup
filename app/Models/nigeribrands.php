<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class nigeribrands extends Model
{
    protected $table='nigeriaelectonicbrands';
    use HasFactory;
}
