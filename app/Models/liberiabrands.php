<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class liberiabrands extends Model
{
    protected $table='fmcgliberiabrands';
    use HasFactory;
}
