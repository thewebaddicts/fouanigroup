<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class guinecontactus extends Model
{
    protected $table='guineacontactus';
    use HasFactory;
}
