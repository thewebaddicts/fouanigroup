<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class nigeriafactory extends Model
{
    protected $table='nigeriafactory';
    use HasFactory;
}
