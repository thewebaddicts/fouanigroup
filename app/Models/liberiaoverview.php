<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class liberiaoverview extends Model
{
    protected $table='liberiahomepageoverview';
    use HasFactory;
}
