<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class installationtypemodel extends Model
{
    protected $table='installationtype';
    use HasFactory;
}
