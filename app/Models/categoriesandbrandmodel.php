<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class categoriesandbrandmodel extends Model
{
    protected $table='brandsandcategoriespage';
    use HasFactory;
}
