<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class lebanonpagetwo extends Model
{
    protected $table='lebanonsecondpage';
    use HasFactory;
}
