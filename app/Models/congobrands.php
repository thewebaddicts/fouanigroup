<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class congobrands extends Model
{
    protected $table='congobrands';
    use HasFactory;
}
