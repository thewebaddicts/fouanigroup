<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class errortypesmodel extends Model
{
    protected $table='errortype';

    use HasFactory;
}
