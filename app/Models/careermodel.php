<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class careermodel extends Model
{
    protected $table='careers';
    use HasFactory;
}
