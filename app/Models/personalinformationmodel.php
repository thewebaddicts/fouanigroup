<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class personalinformationmodel extends Model
{
    protected $table='personalinformation';
    use HasFactory;
}
