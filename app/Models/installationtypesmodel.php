<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class installationtypesmodel extends Model
{
    protected $table='installationtypes';
    use HasFactory;
}
