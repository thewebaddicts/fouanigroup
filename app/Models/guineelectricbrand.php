<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class guineelectricbrand extends Model
{
    protected $table='guineaelectronicbrand';
    use HasFactory;
}
