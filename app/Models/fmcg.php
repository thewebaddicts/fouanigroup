<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class fmcg extends Model
{
    protected $table='nigeriafmcg';
    use HasFactory;
}
