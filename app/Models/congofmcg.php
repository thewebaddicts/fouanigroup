<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class congofmcg extends Model
{
    protected $table='congofmcg';
    use HasFactory;
}
