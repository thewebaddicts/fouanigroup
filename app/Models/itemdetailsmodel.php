<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class itemdetailsmodel extends Model
{
    protected $table="itemdetails";
    use HasFactory;
}
