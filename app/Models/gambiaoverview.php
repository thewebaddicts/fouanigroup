<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class gambiaoverview extends Model
{
    protected $table='gambiaoverview';
    use HasFactory;
}
