<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class homepagecontactus extends Model
{
    protected $table='contactushome';
    use HasFactory;
}
