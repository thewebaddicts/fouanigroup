<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lebanonrealstate extends Model
{
    protected $table='lebanonrealstate';
    use HasFactory;
}
