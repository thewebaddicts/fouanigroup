<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class homepagemainbrandsmodel extends Model
{
    protected $table="homepagemainbrands";
    use HasFactory;
}
