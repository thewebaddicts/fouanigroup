<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class nigeriaoverview extends Model
{
    protected $table='nigeriaoverview';
    use HasFactory;
}
