<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class homepagecountriesmodel extends Model
{
    protected $table= 'homepagecountries';
     use HasFactory;
}
