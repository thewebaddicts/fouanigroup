<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\EmailTemplateModel;
class emailtemplate extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject;
    public $dictionary;
    public function __construct($subject,$dictionary)
    {
        $this->subject=$subject;
        $this->dictionary=$dictionary;
    }
    public function build()
    {
        $template = EmailTemplateModel::where('cancelled',0)->where('location',$this->subject)->first();
        if($template){
            return $this->subject($template->subject)
                ->view('email.template-email', [ "template" => $template, "dictionary"=>$this->dictionary ]);
        }
    }
}