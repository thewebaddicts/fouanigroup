<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $slideshow=\App\Models\homepagemodel::where('cancelled',0)->get();
    $aboutus=\App\Models\hompepageaboutusmodel::where('cancelled',0)->get();
    $mainbrands=\App\Models\homepagemainbrandsmodel::where('cancelled',0)->get();
    $country=\App\Models\homepagecountriesmodel::where('cancelled',0)->get();
    return view('pages.home',['slideshow'=>$slideshow,'aboutus'=>$aboutus,'mainbrands'=>$mainbrands,'country'=>$country]);
});
Route::post('/', [\App\Http\Controllers\contactusController::class, 'contactus'])->name('contact');

Route::get('/nigeriahome', function () {
    $slideshow=\App\Models\nigeriahomepage::where('cancelled',0)->get();
    $overview=\App\Models\nigeriaoverview::where('cancelled',0)->get();
    $brands=\App\Models\nigeribrands::where('cancelled',0)->get();
    $outlits=\App\Models\nigerioutlits::where('cancelled',0)->get();
    $fmcg=\App\Models\fmcg::where('cancelled',0)->get();
    $factory=\App\Models\nigeriafactory::where('cancelled',0)->get();
    return view('pages.Nigeriahome',['slideshow'=>$slideshow,'overview'=>$overview,'brands'=>$brands,'outlits'=>$outlits,'fmcg'=>$fmcg,'factory'=>$factory]);
});
Route::post('/nigeriahome', [\App\Http\Controllers\contactusnigeriaController::class, 'contactusnigeria'])->name('contactusnigera');

Route::get('/brand/{id}', function ($id) {
    $brands=\App\Models\nigeribrands::where('cancelled',0)->where('id',$id)->get();


    return view('pages.lgbrand',['brands'=>$brands]);
});
Route::get('/outlet/{id}', function ($id) {
    $outlits=\App\Models\nigerioutlits::where('cancelled',0)->where('id',$id)->get();
    $categories=\App\Models\showroomcategories::where('cancelled',0)->get();
    $map=\App\Models\map::where('cancelled',0)->get();
   


    return view('pages.showroom',['outlits'=>$outlits,'categories'=>$categories,'map'=>$map]);
});
Route::get('/fmcg/{id}', function ($id) {
    $fmcg=\App\Models\fmcg::where('cancelled',0)->where('id',$id)->get();


    return view('pages.nutrimental',['fmcg'=>$fmcg]);
});
Route::get('/manufacturing', function () {
    $manu=\App\Models\manufacturingniger::where('cancelled',0)->get();
    $categories=\App\Models\manufacturingnigeriacategories::where('cancelled',0)->get();
    return view('pages.manufacturing',['manu'=>$manu,'categories'=>$categories]);
});
Route::get('/careers', function () {
    $careers=\App\Models\careermodel::where('cancelled',0)->get();
    return view('pages.careers',['careers'=>$careers]);
});
Route::post('/careers', [\App\Http\Controllers\careermodelformcontroller::class, 'careerpage'])->name('career');

Route::get('/contactus', function () {
    $contactus=\App\Models\contactus::where('cancelled',0)->get();
    return view('pages.contactus',['contactus'=>$contactus]);
});
Route::post('/contactus', [\App\Http\Controllers\contactuspage::class, 'contactuspage'])->name('contactuspage');

Route::get('/liberia',function(){
    $slideshow=\App\Models\liberiahome::where('cancelled',0)->get();
    $overview=\App\Models\liberiaoverview::where('cancelled',0)->get();
    $brands=\App\Models\liberiaelectricbrand::where('cancelled',0)->get();
    $fmcg=\App\Models\liberianfmcg::where('cancelled',0)->get();
    $item=\App\Models\liberiapackgingtitle::where('cancelled',0)->get();

    return view('pages.liberiahome',['slideshow'=>$slideshow,'overview'=>$overview,'brands'=>$brands,'fmcg'=>$fmcg,'item'=>$item]);


});
Route::post('/liberia', [\App\Http\Controllers\liberiacontactus::class, 'contactusliberia'])->name('contactusliberia');

Route::get('/fmcgliberia',function(){
    $fmcg=\App\Models\fmcg::where('cancelled',0)->get();
    $fmcgslideshow=\App\Models\fmcgslideshowmodel::where('cancelled',0)->get();
    $fmcgbrands=\App\Models\liberiabrands::where('cancelled',0)->get();
    

    return view('pages.fmcgliberia',['fmcg'=>$fmcg,'fmcgslideshow'=>$fmcgslideshow,'fmcgbrands'=>$fmcgbrands]);


});

Route::get('/congohome',function(){
    $fmcg=\App\Models\congofmcg::where('cancelled',0)->get();
    
    $brands=\App\Models\congobrands::where('cancelled',0)->get();


    $slideshow=\App\Models\congohomepage::where('cancelled',0)->get();
    $over=\App\Models\congooverview::where('cancelled',0)->get();
    $factory=\App\Models\congofactory::where('cancelled',0)->get();
    return view('pages.congohome',['slideshow'=>$slideshow,'over'=>$over,'factory'=>$factory,'fmcg'=>$fmcg,'brands'=>$brands]);


});
Route::post('/congohome', [\App\Http\Controllers\congocontactuscontroller::class, 'contactuscongo'])->name('contactuscongo');

Route::get('/guinea',function(){
    $slideshow=\App\Models\guineahomepage::where('cancelled',0)->get();
    $overview=\App\Models\guineaoverview::where('cancelled',0)->get();
    $fmcg=\App\Models\guineafmcg::where('cancelled',0)->get();
    $brand=\App\Models\guineelectricbrand::where('cancelled',0)->get();
    $factory=\App\Models\guinefactory::where('cancelled',0)->get();

    return view('pages.guinea',['slideshow'=>$slideshow,'overview'=>$overview,'fmcg'=>$fmcg,'brand'=>$brand,'factory'=>$factory]);


});
Route::post('/guinea', [\App\Http\Controllers\guinecontroller::class, 'contactusguinea'])->name('contactusguinea');

Route::get('/gambia',function(){
    $slideshow=\App\Models\gambiahome::where('cancelled',0)->get();
    $overview=\App\Models\gambiaoverview::where('cancelled',0)->get();
    $brand=\App\Models\gambiaelectricbrands::where('cancelled',0)->get();
    $fmcg=\App\Models\gambiafmcg::where('cancelled',0)->get();

    return view('pages.gambia',['slideshow'=>$slideshow,'overview'=>$overview,'brand'=>$brand,'fmcg'=>$fmcg]);


});
Route::post('/gambia', [\App\Http\Controllers\gambiacontroller::class, 'contactusgambia'])->name('contactusgambia');

Route::get('/lebanon',function(){
    $slideshow=\App\Models\Lebanon::where('cancelled',0)->get();
    $overview=\App\Models\Lebanonoverview::where('cancelled',0)->get();
    $item=\App\Models\Lebanonrealstate::where('cancelled',0)->get();
    return view('pages.lebanon',['slideshow'=>$slideshow,'overview'=>$overview,'item'=>$item]);


});
Route::post('/lebanon', [\App\Http\Controllers\lebanoncontactus::class, 'contactuslebanon'])->name('contactuslebanon');

Route::get('/lebanonrealstate',function(){
    $slideshow=\App\Models\lebanonpagetwo::where('cancelled',0)->get();
    $categories=\App\Models\lebanongallery::where('cancelled',0)->get();
    return view('pages.realstate',['slideshow'=>$slideshow,'categories'=>$categories]);


});
Route::get('/service',function(){
    $service=\App\Models\Nigeriaservice::where('cancelled',0)->get();
    return view('pages.service',['service'=>$service]);


});
Route::get('/installation',function(){
    $categories=\App\Models\categoriesandbrandmodel::where('cancelled',0)->get();
    return view('pages.request',['categories'=>$categories]);


});
Route::post('/installation', [\App\Http\Controllers\requestformcontroller::class, 'requestpage'])->name('requestpage');

Route::get('/inhomeservice',function(){
    return view('pages.inhome');


});
Route::get('/categories',function(){
    $categories=\App\Models\categoriesandbrandmodel::where('cancelled',0)->get();
    $brands=\App\Models\electronicsbrandsmodel::where('cancelled',0)->get();

    return view('pages.categories',['categories'=>$categories,'brands'=>$brands]);


});
Route::post('/categories', [\App\Http\Controllers\brandsandcategoriescontroller::class, 'brandsandcategories'])->name('brandsandcategories');

Route::get('/items',function(){
    $item= \App\Models\itemsmodel::where('cancelled',0)->get();
    return view('pages.items',['item'=>$item]);


});
Route::post('/items', [\App\Http\Controllers\itemdetailscontroller::class, 'itemdetails'])->name('itemsdetails');

Route::get('/itemdetails',function(){
    $item=\App\Models\installationtypesmodel::where('cancelled',0)->get();
    return view('pages.itemsdetails',['item'=>$item]);


});
Route::post('/itemdetails', [\App\Http\Controllers\installationtypecontroller::class, 'installtiontype'])->name('installation');


Route::get('/errordetails',function(){
    $error=\App\Models\errortypemodel::where('cancelled',0)->get();
    return view('pages.error',['error'=>$error]);


});
Route::post('/errordetails', [\App\Http\Controllers\errortypecontroller::class, 'errortype'])->name('errortype');

Route::get('/errordetailspage',function(){
    $error=\App\Models\errordetails::where('cancelled',0)->get();

    return view('pages.errorpage',['error'=>$error]);


});
Route::post('/errordetailspage', [\App\Http\Controllers\personalinformationcontroller::class, 'personalinformation'])->name('personalinfo');

Route::get('/personalinformation',function(){
    // $personal=\App\Models\personalinformationmodel::where('cancelled',0)->get();
    return view('pages.personalinfo');


});
Route::get('/summary',function(){
    $brand=\App\Models\brandsandcategories::where('cancelled',0)->latest()->first();
    
    $items=\App\Models\itemdetailsmodel::where('cancelled',0)->latest()->first();;
    
    $types=\App\Models\installationtypemodel::where('cancelled',0)->latest()->first();;
    
    $error=\App\Models\errortypesmodel::where('cancelled',0)->latest()->first();;
    
    $personal=\App\Models\personalinformationmodel::where('cancelled',0)->latest()->first();;
    
    return view('pages.summary',['brand'=>$brand,'items'=>$items,'types'=>$types,'error'=>$error,'personal'=>$personal]);


});
Route::get('/submitrequest',function(){
    return view('pages.submit');


});
Route::get('/liberiabrands/{id}',function($id){
    $brands=\App\Models\liberiaelectricbrand::where('cancelled',0)->where('id',$id)->get();

    return view('pages.liberiabrand',['brands'=>$brands]);


});
Route::get('/congoelectronicbrands/{id}',function($id){
    $brands=\App\Models\congobrands::where('cancelled',0)->where('id',$id)->get();

    return view('pages.congobrands',['brands'=>$brands]);


});
Route::get('/guineaelectronicbrands/{id}',function($id){
    $brands=\App\Models\guineelectricbrand::where('cancelled',0)->where('id',$id)->get();

    return view('pages.guineabrands',['brands'=>$brands]);


});
Route::get('/gambiaelectronicbrands/{id}',function($id){
    $brands=\App\Models\gambiaelectricbrands::where('cancelled',0)->where('id',$id)->get();

    return view('pages.gambiabrands',['brands'=>$brands]);



});
Route::get('/nigeriafactory',function(){
    $slideshow=\App\Models\nigeriafactory::where('cancelled',0)->get();

    return view('pages.nigeriafactory',['slideshow'=>$slideshow]);


});
Route::get('/liberiapackaging',function(){
    $slideshow=\App\Models\liberiapackgingtitle::where('cancelled',0)->get();

    return view('pages.liberiapackaging',['slideshow'=>$slideshow]);


});
Route::get('/congomeuble',function(){
    $slideshow=\App\Models\congofactory::where('cancelled',0)->get();

    return view('pages.congopackaging',['slideshow'=>$slideshow]);



});
Route::get('/guineaimmobilier',function(){
    $slideshow=\App\Models\guinefactory::where('cancelled',0)->get();

    return view('pages.guineafactory',['slideshow'=>$slideshow]);


});
Route::get('/liberianfmcg/{id}',function($id){
    $fmcg=\App\Models\liberianfmcg::where('cancelled',0)->where('id',$id)->get();

    return view('pages.liberianfmcg',['fmcg'=>$fmcg]);


});
Route::get('/congofmcg/{id}',function($id){
    $fmcg=\App\Models\congofmcg::where('cancelled',0)->where('id',$id)->get();

    return view('pages.congofmcg',['fmcg'=>$fmcg]);


});
Route::get('/gambiafmcg/{id}',function($id){
    $fmcg=\App\Models\gambiafmcg::where('cancelled',0)->where('id',$id)->get();

    return view('pages.gambiafmcg',['fmcg'=>$fmcg]);


});
Route::get('/guineafmcg/{id}',function($id){
    $fmcg=\App\Models\guineafmcg::where('cancelled',0)->where('id',$id)->get();

    return view('pages.guineafmcg',['fmcg'=>$fmcg]);


});






