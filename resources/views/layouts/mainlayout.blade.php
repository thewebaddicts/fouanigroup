<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="store-id" content="{{ request()->segment(1) }}">
    <meta name="lang" content="{{ request()->segment(2) }}">
    <link rel="stylesheet" href="path/to/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Archivo:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">

    <title>Fouani Group</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,800;1,900&display=swap"
        rel="stylesheet">
    <link href="https://api.mapbox.com/mapbox-gl-js/v2.8.2/mapbox-gl.css" rel="stylesheet">
    <script src="https://api.mapbox.com/mapbox-gl-js/v2.8.2/mapbox-gl.js"></script>
    <style>
        .common-loader-initial {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            width: 100vw;
            height: 100vh;
            object-fit: cover;
            margin: auto;
            background-color: #fff;
            z-index: 9999;
            background-image: url("/images/loader.gif");
            background-position: center;
            background-repeat: no-repeat;
            background-size: 450px;
        }


        body,
        html {
            margin: 0;
            padding: 0;
        }

    </style>
    @yield('title')
</head>

<body>





    @yield('content')

    <script language="javascript" src="/js/require.js?v={{ env('CACHE_VERSION') }}"></script>
    <script language="javascript" src="/js/init.js?v={{ env('CACHE_VERSION') }}"
        attr-cache-version="{{ env('CACHE_VERSION') }}"></script>

    <script language="javascript">
        function NotificationFunction() {
            @if (request()->input('notification'))
                ShowMessage("","{{ request()->input('notification') }}");
                removeQueryNotificationString();
            @endif
            @if (request()->input('notification_title') && request()->input('notification_message'))
                ShowMessage("{{ request()->input('notification_title') }}","{{ request()->input('notification_message') }}");
                removeQueryNotificationString();
            @endif
            @if (request()->input('notification_id'))
                <?php $notification = \App\Models\NotificationModel::getNotification(request()->input('notification_id'));
                ?>
                ShowMessage("{{ $notification->label }}","{{ $notification->text }}");
                removeQueryNotificationString();
            @endif
        }
    </script>
</body>


</html>
