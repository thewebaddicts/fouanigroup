@extends('layouts.mainlayout') @section('title')
    <title> Lebanon Home Page </title>
@endsection
@extends('mainmenu.mainthree')
@section('content')
    <div class="right">
        <div class="socialmedia">
            <div class="youtube">
                <i class="fa-solid fa-store"></i>
            </div>
            <div class="facebook">
                <i class="fa-brands fa-facebook-f"></i>
            </div>

            <div class="instagram">
                <i class="fa-brands fa-instagram"></i>
            </div>

        </div>
    </div>
    <div style="padding-top:120px">

        <div class="hometitlebanon contant">
            <div class="home"> Home/</div>
            <div class="nigeria">Nigeria/</div>
            <div class="service">Service Request/</div>
            <div class="real">In-home Service</div>

        </div>

        <div class="rednavbar">
            <div class="allcircles">
                <div class="card">
                    <div class="circle">
                        1

                    </div>
                    <div class="text1">
                        Brand & Category
                    </div>
                </div>
                <div class="card">
                    <div class="circle-2">
                        2

                    </div>
                    <div class="text2">
                        Item Details
                    </div>
                </div>
                <div class="card">

                    <div class="circle-3">
                        3

                    </div>
                    <div class="text3">
                        Error Details
                    </div>
                </div>
                <div class="card">
                    <div class="circle-4">
                        4

                    </div>
                    <div class="text4">
                        Personal Information
                    </div>
                </div>

                <div class="card">
                    <div class="circle-5">
                        5

                    </div>
                    <div class="text5">
                        Request Summary
                    </div>
                </div>




            </div>

        </div>
        <div class="inhometext">
            <div class="text">
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
                dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
                clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
            </div>
        </div>
        <form action="{{ route('brandsandcategories') }}" method="POST">
            @csrf
            <div class="brands">
                <div class="allbrands">
                    <div class="title">Select Brand</div>
                    <div class="allcards">
                        @foreach ($categories as $category)
                            <div class="card  brand4">
                                <img src="{{ env('DATA_URL') }}/brandsandcategoriespage/{{ $category->id }}.{{ $category->extension_image }}?v={{ $category->version }}"
                                    alt="">
                                <input required class="thisinput" style="display: none" value="{{ $category->brandname }}" type="radio" name="brand"
                                    id="">
                            </div>
                        @endforeach
    
                    </div>
                </div>
            </div>
            <div class="categoriess">
                <div class="title">Choose Category</div>
                <div class="allcards">
                    @foreach ($brands as $brand)
                        <div class="card brand5">
                            <div class="cardimage">
                                <img src="{{ env('DATA_URL') }}/electrical/{{ $brand->id }}.{{ $brand->extension_svg }}?v={{ $brand->version }}" alt="">
                                <input required class="thisinputs" style="display: none" value="{{ $brand->label }}" type="radio" name="category"
                                    id="">
                              
    
                            </div>
                            <div class="text">{{ $brand->label }}</div>
                        </div>
                    @endforeach
    
    
    
    
                </div>
                <div class="button">
                  <button style="border: 0px; background-color:transparent; color:white; font-weight:800"> Next Step <i class="fa-solid fa-chevron-right"></i></button> 
                </div>
            </div>
        </form>
      
        


    </div>
    @component('footer.footer')
    @endcomponent
@endsection
