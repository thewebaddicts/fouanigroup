@extends('layouts.mainlayout') @section('title')
    <title> Nigeria Home Page </title>
@endsection
@extends('mainmenu.liberiamenu')
@section('content')
    <div class="right">
        <div class="socialmedia">

            <div class="facebook">
                <i class="fa-brands fa-facebook-f"></i>
            </div>

            <div class="instagram">
                <i class="fa-brands fa-instagram"></i>
            </div>

        </div>
    </div>

    <div class="fmcgliberia">
        @foreach ($fmcgslideshow as $slide)
        @endforeach
        <div class="slide"
            style="    background-image: linear-gradient(
                            rgba(0, 0, 0, 0.527),
                            rgba(0, 0, 0, 0.5)
                        ),
                        url('{{ env('DATA_URL') }}/fmcgliberia/{{ $slide->id }}.{{ $slide->extension_slideshowimage }}?v={{ $slide->version }}');">

            <div class="slidecontent">
                <div class="title">{{ $slide->slideshowlabel }}</div>

            </div>
        </div>
    </div>
    <div class="hometitlelg contant">
        <div class="home"> Home/</div>
        <div class="nigeria">Liberia/</div>
        <div class="lg">FMCG</div>
    </div>
    <div class="nutri">


        <div class="nutricontent">
            @foreach ($fmcgbrands as $brand)
                @php
                    $brand = json_decode($brand->fmcgbrands);
                @endphp
                @foreach ($brand as $card)
                    <div class="cards">


                        <div class="n-image">
                            <img src="{{ env('DATA_URL') . $card->image }}" alt="">

                        </div>
                        <div class="alltext">
                            <div class="title">{{ $card->label }}</div>
                            <div class="textone">{{ $card->textone }}</div>
                            <div class="textone">{{ $card->texttwo }}
                            </div>
                            <div class="semititle-1">{{ $card->subtitle }}
                            </div>
                            <div class="textone"> {{ $card->textthree }}
                            </div>
                            <div class="semititle-1">{{ $card->subtitletwo }}
                            </div>
                            <div class="textone">{{ $card->textfour }}</div>
                        </div>

                    </div>
                @endforeach
            @endforeach







        </div>

        <div class="itemscarousel">
            <div class="allcards">
                @foreach ($fmcgbrands as $fmcbrand)
                    @php
                        $fmcbrand = json_decode($fmcbrand->electronicbrands);
                    @endphp
                    @foreach ($fmcbrand as $cards)
                        <div class="card">
                            <img src="{{ env('DATA_URL') . $cards->image }}" alt="">
                        </div>
                    @endforeach
                @endforeach









            </div>
        </div>

        @component('footer.footer')
        @endcomponent
    @endsection
