@extends('layouts.mainlayout') @section('title')
    <title> Congo Home Page </title>
@endsection
@extends('mainmenu.congomenu')
@section('content')
    <div class="right">
        <div class="socialmedia">
            <div class="youtube">
                <i class="fa-solid fa-store"></i>
            </div>
            <div class="facebook">
                <i class="fa-brands fa-facebook-f"></i>
            </div>

            <div class="instagram">
                <i class="fa-brands fa-instagram"></i>
            </div>

        </div>
    </div>
    <div class="congohome">
        @foreach ($slideshow as $slide)
            <div class="slide"
                style="   background-image: linear-gradient(
                                    rgba(0, 0, 0, 0.527),
                                    rgba(0, 0, 0, 0.5)
                                ),
                                url('{{ env('DATA_URL') }}/congohomepage/{{ $slide->id }}.{{ $slide->extension_slideshow }}?v={{ $slide->version }}');">
                <div class="slidecontent">
                    <div class="title">{{ $slide->label }}</div>
                    <a href="{{ $slide->link }}">
                        <div class="store">
                            {{ $slide->congobutton }}
                        </div>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
    <div class="hometitlecongo contant">
        <div class="home"> Accueil/</div>
        <div class="nigeria">Congo</div>

    </div>

    <div class="overview">
        <div class="overviewcontent">
            @foreach ($over as $overview)
                <div class="title">{{ $overview->label }}</div>
            @endforeach
            @foreach ($over as $cards)
                <div class="overview2">
                    <div class="overviewimage">
                        <img src=" {{ env('DATA_URL') }}/congooverview/{{ $slide->id }}.{{ $slide->extension_slideshow }}?v={{ $slide->version }}"
                            alt="">
                    </div>
                    <div class="overviewtext">


                        @php
                            $cards = json_decode($cards->overviewdetails);
                        @endphp
                        @foreach ($cards as $card)
                            <div class="overviewtitle">{{ $card->label }}</div>
                            <div class="text">
                                <div class="textone"> {{ $card->textone }}
                                </div>
                                <div class="texttwo">{{ $card->texttwo }}</div>
                                <div class="textthree">
                                    {{ $card->textthree }}
                                </div>
                            </div>
                        @endforeach


                    </div>
            @endforeach
        </div>
    </div>
    </div>
    <div class="electronicsbrandsliberia">
        <div class="electronicscontent">
            <div class="title">Electronics Brands</div>
            <div class="text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                doloremque laudantium,
                totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
                explicabo. Nemo enim ipsam voluptatem quia voluptas.</div>
            <div class="location">
                <div class="loc-1"><i class="fa-solid fa-location-dot"></i></div>
                <div class="text-2">Showroom Located in
                    City, Country Floor, Building, Street</div>
            </div>
            <div class="allcard carousel owl-carousel" data-carousel-items="2" >
                @foreach ($brands as $brand)
                    <a href="/congoelectronicbrands/{{ $brand->id }}">
                        <div class="card">
                            <div class="cardimage">
                                <img src="{{ env('DATA_URL') }}/congobrands/{{ $brand->id }}.{{ $brand->extension_coverimage }}?v={{ $brand->version }}"
                                    alt="">

                            </div>
                            <div class="cardimage2">
                                <img class="lg"
                                    src="{{ env('DATA_URL') }}/congohoverimage/{{ $brand->id }}.{{ $brand->extension_imageonhover }}?v={{ $brand->version }}"
                                    alt="">
                            </div>
                        </div>
                    </a>
                @endforeach


            </div>

        </div>
    </div>
    <div class="fmcg">
        <div class="fmcgcontent">
            <div class="title">FMCG</div>
            <div class="text">
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
                explicabo. Nemo enim ipsam voluptatem quia voluptas.
            </div>
            <div class="allcards carousel owl-carousel" data-carousel-items="2">
                @foreach ($fmcg as $card)
                    <div class="card">
                        <a href="/congofmcg/{{ $card->id }}">
                            <div class="cardimage">
                                <img src="{{ env('DATA_URL') }}/congofmcgcoverimage/{{ $card->id }}.{{ $card->extension_coverimage }}?v={{ $card->version }}"
                                    alt="">
                            </div>
                        </a>



                    </div>
                @endforeach




            </div>

        </div>
    </div>
    <div class="factorysection carousel owl-carousel" data-carousel-items="1" data-carousel-dots="true">
        @foreach ($factory as $factories)
            <div class="factorycontent" style="background-image: linear-gradient(
                rgba(0, 0, 0, 0.527),
                rgba(0, 0, 0, 0.5)
            ),
            url('{{ env('DATA_URL') }}/meuble/{{ $factories->id }}.{{ $factories->extension_image }}?v={{ $factories->version }}');">
                <div class="title">{{ $factories->label }}</div>
                <div class="text">{{ $factories->text }}</div>
                <div class="learnmore">
                    <a  target="_blank" href="/congomeuble">{{ $factories->button }}</a>
                </div>
            </div>
        @endforeach

    </div>
    <div class="contactus">
        <div class="contactuscontent">
            <div class="title">
                Contact

            </div>
            <div class="contactusform">
                <form action="{{ route('contactuscongo') }}" method="POST">
                    @csrf

                    <div class="row">
                        <div class="inputone">
                            <div class="firstname">Prénom <div class="star">*</div>
                            </div>
                            <input required name="firstname" type="text" placeholder="First Name">
                        </div>
                        <div class="inputtwo">
                            <div class="firstname">Nom <div class="star">*</div>
                            </div>

                            <input required name="lastname" type="text" name="" id="" placeholder="Last Name">
                        </div>
                    </div>
                    <div class="row">

                        <div class="inputone">
                            Adresse e-mail



                            <input required name="emailaddress" type="email" placeholder="Email address">
                        </div>
                        <div class="inputtwo">
                            Pays
                            {{-- <input name="country" class="nigeria33" type="text" placeholder="Congo" value=""> --}}
                            <select required class="nigeria33" name="country" id="">
                                <option value="">Congo</option>
                                <option value="">Congo</option>
                            </select>


                        </div>
                    </div>
                    <div class="row">
                        <div class="inputthree">
                            Message
                            <input required name="message" type="text" placeholder="Message">
                        </div>
                    </div>
                    <div class="submit">
                        <input required type="submit" name="" id="" value="Send Message">
                    </div>
                </form>
            </div>
        </div>
    </div>
