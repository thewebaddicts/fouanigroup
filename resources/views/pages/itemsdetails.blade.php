@extends('layouts.mainlayout') @section('title')
    <title> Lebanon Home Page </title>
@endsection
@extends('mainmenu.mainthree')
@section('content')
    <div class="right">
        <div class="socialmedia">
            <div class="youtube">
                <i class="fa-solid fa-store"></i>
            </div>
            <div class="facebook">
                <i class="fa-brands fa-facebook-f"></i>
            </div>

            <div class="instagram">
                <i class="fa-brands fa-instagram"></i>
            </div>

        </div>
    </div>
    <div style="padding-top:120px">

        <div class="hometitlebanon contant">
            <div class="home"> Home/</div>
            <div class="nigeria">Nigeria/</div>
            <div class="service">Service Request/</div>
            <div class="real">In-home Service</div>

        </div>

        <div class="rednavbar2">
            <div class="allcircles">
                <div class="card">
                    <div class="circle">
                        1

                    </div>
                    <div class="text1">
                        Brand & Category
                    </div>
                </div>
                <div class="card">
                    <div class="circle-2">
                        2

                    </div>
                    <div class="text2">
                        Item Details
                    </div>
                </div>
                <div class="card">

                    <div class="circle-3">
                        3

                    </div>
                    <div class="text3">
                        Error Details
                    </div>
                </div>
                <div class="card">
                    <div class="circle-4">
                        4

                    </div>
                    <div class="text4">
                        Personal Information
                    </div>
                </div>

                <div class="card">
                    <div class="circle-5">
                        5

                    </div>
                    <div class="text5">
                        Request Summary
                    </div>
                </div>




            </div>

        </div>


        <div class="items">
            <div class="title">Select Outdoor Unit Installation Type </div>

        </div>
        <form action="{{ route('installation') }}" method="POST">
            @csrf
            <div class="outdoorunit">
                <div class="allcards">
                    @foreach ($item as $card)

                        <div class="card brand5">
                            <div class="cardimage"><img
                                    src="{{ env('DATA_URL') }}/installationtype/{{ $card->id }}.{{ $card->extension_image }}?v={{ $card->version }}"
                                    alt=""></div>

                            <div class="text">{{ $card->text }}</div>

                            <div class="mask">
                            </div>
                            <input required class="thisinput" style="display: none" type="radio" name="type" id="" value="{{ $card->text }}">
                        </div>
                    @endforeach

                </div>
            </div>



            <div class="submiting">
                <div> <i style="    position: relative;
                                    left: 18px;
                                    top: 2px; color:#e51240" class="fa-solid fa-chevron-left"></i> <input
                        class="ps" type="submit" name="" id="" value="Previous Step"></div>
                <div><input class="ns" type="submit" name="" id="" value="Next Step"><i style="    position: relative;
                                    right: 24px;
                           
                                    top: 2px;
                                    color: white;" class="fa-solid fa-chevron-right"></i></div>
            </div>
        </form>

    </div>

    @component('footer.footer')
    @endcomponent
@endsection
