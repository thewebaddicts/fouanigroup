@extends('layouts.mainlayout') @section('title')
    <title> Nigeria Home Page </title>
@endsection
@extends('mainmenu.liberiamenu')
@section('content')
    <div class="right">
        <div class="socialmedia">
            <div class="youtube">
                <i class="fa-solid fa-store"></i>
            </div>
            <div class="facebook">
                <i class="fa-brands fa-facebook-f"></i>
            </div>

            <div class="instagram">
                <i class="fa-brands fa-instagram"></i>
            </div>

        </div>
    </div>
    <div class="lgbrands">
        @foreach ($brands as $brand)
            <div class="slide"
                style="   background-image: linear-gradient(
                                rgba(0, 0, 0, 0.527),
                                rgba(0, 0, 0, 0.5)
                            ),
                            url('{{ env('DATA_URL') }}/liberiaelectronic/{{ $brand->id }}.{{ $brand->extension_slideshwoimage }}?v={{ $brand->version }}');">
                <div class="slidecontent">
                    <div class="title">{{ $brand->label }}</div>
                    <a href="{{ $brand->link }}">
                        <div class="store">
                            {{ $brand->button }}
                        </div>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
    <div class="hometitlelg contant">
        <div class="home"> Home/</div>
        <div class="nigeria">Liberia/</div>
        @foreach ($brands as $brand)
            <div class="lg">{{ $brand->brandname }}</div>
        @endforeach
    </div>

    <div class="lgelectronics">
        <div class="lgelectronicscontent">
            @foreach ($brands as $electronics)
                <div class="container">
                    <div class="title">{{ $electronics->electronicbrandlabel }}</div>
                    <div class="alltexts">
                        <div class="text-1"> {{ $electronics->electronicbrandtextone }}
                        </div>
                        <div class="text-2"> {{ $electronics->electronicbrandtexttwo }}
                        </div>
                    </div>
                    <div class="buttons">
                        <a href="{{ $electronics->electronicbrandbuttononelink }}" target="_blank">{{ $electronics->electronicbrandbuttonone }}</a>
                        <a href="{{ $electronics->electronicbrandbuttonlinktwo}}" target="_blank">{{ $electronics->electronicbrandbuttontwo }}</a>
                        <a href="{{ $electronics->buttonthreelink }}"
                            target="_blank">{{ $electronics->buttonthree }}</a>
                    </div>
                </div>




                <div class="lgimage">
                    <img src="{{ env('DATA_URL') }}/bradimage/{{ $electronics->id }}.{{ $electronics->extension_electronicbrandsimage }}?v={{ $electronics->version }}"
                        alt="">
                </div>
            @endforeach
        </div>
    </div>

    <div class="categories">
        <div class="title">Categories</div>
        <div class="allcards carousel owl-carousel " data-carousel-items="5" data-carousel-nav="true">
            @foreach ($brands as $cards)
                @php
                    $cards = json_decode($cards->categories);
                @endphp
                @foreach ($cards as $card)
                    <div class="card">
                        <div class="mask">
                            <div class="label">{{ $card->label }}</div>
                        </div>
                        <div class="card-image">
                            <img src="{{ env('DATA_URL') . $card->image }}" alt="">
                        </div>
                    </div>
                @endforeach
            @endforeach


        </div>
    </div>






    @component('footer.footer')
    @endcomponent
@endsection
