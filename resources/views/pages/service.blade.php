@extends('layouts.mainlayout') @section('title')
    <title> Lebanon Home Page </title>
@endsection
@extends('mainmenu.menutwo')
@section('content')
    <div class="right">
        <div class="socialmedia">
            <div class="youtube">
                <i class="fa-solid fa-store"></i>
            </div>
            <div class="facebook">
                <i class="fa-brands fa-facebook-f"></i>
            </div>

            <div class="instagram">
                <i class="fa-brands fa-instagram"></i>
            </div>

        </div>
    </div>
    <div class="lebanonrealstate">
        @foreach ($service as $slide)
            <div class="slide"
                style="   background-image: linear-gradient(
                            rgba(0, 0, 0, 0.527),
                            rgba(0, 0, 0, 0.5)
                        ),
                        url('{{ env('DATA_URL') }}/serviceslideshow/{{ $slide->id }}.{{ $slide->extension_serviceslideshow }}?v={{ $slide->version }}');">
                <div class="slidecontent">
                    <div class="title">{{ $slide->label }}</div>

                </div>
            </div>
        @endforeach
    </div>
    <div class="hometitlebanon contant">
        <div class="home"> Home/</div>
        <div class="nigeria">Nigeria/</div>
        <div class="real">Service Request</div>

    </div>
    <div class="overviewservices" style="padding-bottom: 150px">
        <div class="overviewcontent">
            @foreach ($service as $install)
                @php
                    $install = json_decode($install->installationrequest);
                @endphp
                @foreach ($install as $request)
                    <div class="overview2">

                        <div class="overviewtext">
                            <div class="overviewtitle">{{ $request->title }}</div>
                            <div class="text">
                                <div class="textone">{{ $request->textone }}
                                </div>
                                <div class="texttwo"> {{ $request->texttwo }}</div>
                                <div class="textthree">
                                    <div class="address">
                                        <div class="addresscontent">

                                            <div class="location">
                                                <div class="loc-1"><i class="fa-solid fa-location-dot"></i></div>
                                                <div class="text">{{ $request->location }}</div>
                                            </div>
                                            <div class="number">
                                                <div class="phone"><i class="fa-solid fa-phone"></i></div>
                                                <div class="phonetext">{{ $request->phone }} </div>
                                            </div>
                                            <div class="email">
                                                <div class="email-2"><i class="fa-solid fa-envelope"></i></div>
                                                <div class="emailtext">{{ $request->email }}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="{{ $request->buttonlink }}">
                                        <div class="button">
                                            {{ $request->button }}
                                        </div>
                                    </a>
                                </div>
                            </div>

                        </div>
                        <div class="overviewimage">
                            <img src="{{ env('DATA_URL') . $request->image }}" alt="">
                        </div>
                    </div>
                @endforeach
            @endforeach

        </div>
    </div>


    <div class="overviewservices" style="padding-bottom: 150px">
        <div class="overviewcontent">

            <div class="overview2">
                <div class="overviewimage">
                    <img src="/images/serv.png" alt="">
                </div>
                <div class="overviewtext">
                    <div class="overviewtitle">In-home Service</div>
                    <div class="text">
                        <div class="textone">Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                            accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                            veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                            voluptas.
                        </div>
                        <div class="texttwo">Ded quia consequuntur magni dolores eos qui ratione voluptatem sequi
                            nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci
                            velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam
                            quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem.</div>
                        <div class="textthree">
                            <div class="address">
                                <div class="addresscontent">

                                    <div class="location">
                                        <div class="loc-1"><i class="fa-solid fa-location-dot"></i></div>
                                        <div class="text">22 Burma Road Apapa, Lagos State</div>
                                    </div>
                                    <div class="number">
                                        <div class="phone"><i class="fa-solid fa-phone"></i></div>
                                        <div class="phonetext">01 6334444 - 01 888 4444 </div>
                                    </div>
                                    <div class="email">
                                        <div class="email-2"><i class="fa-solid fa-envelope"></i></div>
                                        <div class="emailtext">support@fouani.com</div>
                                    </div>
                                </div>
                            </div>
                            <div class="button">
                                Send Request
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>





    @component('footer.footer')
    @endcomponent
    <script>

    </script>
