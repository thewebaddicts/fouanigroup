@extends('layouts.mainlayout') @section('title')
    <title> Nigeria Home Page </title>
@endsection
@extends('mainmenu.liberiamenu')
@section('content')
    <div class="right">
        <div class="socialmedia">

            <div class="facebook">
                <i class="fa-brands fa-facebook-f"></i>
            </div>

            <div class="instagram">
                <i class="fa-brands fa-instagram"></i>
            </div>

        </div>
    </div>
    <div class="Liberiahomepage">
        @foreach ($slideshow as $slide)
            <div class="slide"
                style="      background-image: linear-gradient(
                            rgba(0, 0, 0, 0.527),
                            rgba(0, 0, 0, 0.5)
                        ),
                        url('{{ env('DATA_URL') }}/liberiahomepage/{{ $slide->id }}.{{ $slide->extension_slideshowimage }}?v={{ $slide->version }}');">
                <div class="slidecontent">
                    <div class="title">{{ $slide->slideshowlabel }}</div>
                    <a href="{{ $slide->link }}">
                        <div class="store">
                            {{ $slide->button }}
                        </div>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
    <div class="hometitle contant">
        <div class="home"> Home/</div>
        <div class="nigeria">Liberia</div>
    </div>
    <div class="overview">
        <div class="overviewcontent">
            <div class="title">Overview</div>
            <div class="overview2">
                @foreach ($overview as $over)
                    <div class="overviewimage">

                        <img src="{{ env('DATA_URL') }}/liberiaoverview/{{ $over->id }}.{{ $over->extension_image }}?v={{ $slide->version }}"
                            alt="">
                    </div>
                    <div class="overviewtext">
                        <div class="overviewtitle">{{ $over->label }}</div>
                        <div class="text">
                            <div class="textone"> {{ $over->textone }}
                            </div>
                            <div class="texttwo"> {{ $over->texttwo }}</div>
                            <div class="textthree">
                                {{ $over->textthree }}
                            </div>
                        </div>

                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="electronicsbrandsliberia">
        <div class="electronicscontent">
            <div class="title">Electronics Brands</div>
            <div class="text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                doloremque laudantium,
                totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
                explicabo. Nemo enim ipsam voluptatem quia voluptas.</div>
            <div class="location">
                <div class="loc-1"><i class="fa-solid fa-location-dot"></i></div>
                <div class="text-2">Showroom Located in
                    City, Country Floor, Building, Street</div>
            </div>
            <div class="allcard">
                @foreach ($brands as $brand)
                    <a href="/liberiabrands/{{ $brand->id }}">
                        <div class="card">
                            <div class="cardimage">
                                <img src="{{ env('DATA_URL') }}/liberiaelectronics/{{ $brand->id }}.{{ $brand->extension_cardimage }}?v={{ $brand->version }}"
                                    alt="">

                            </div>
                            <div class="cardimage2">
                                <img class="lg"
                                    src="{{ env('DATA_URL') }}/liberiaelectronicc/{{ $brand->id }}.{{ $brand->extension_cardimagehover }}?v={{ $brand->version }}"
                                    alt="">
                            </div>
                        </div>
                    </a>
                @endforeach


            </div>

        </div>
    </div>

    <div class="fmcg">
        <div class="fmcgcontent">
            <div class="title">FMCG</div>
            <div class="text">
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
                explicabo. Nemo enim ipsam voluptatem quia voluptas.
            </div>
            <div class="allcards carousel owl-carousel" data-carousel-items="2">
                @foreach ($fmcg as $card)
                    <div class="card">
                        <a href="/liberianfmcg/{{ $card->id }}">
                            <div class="cardimage">
                                <img src="{{ env('DATA_URL') }}/liberianfmcgcover/{{ $card->id }}.{{ $card->extension_coverimage }}?v={{ $card->version }}"
                                    alt="">
                            </div>
                        </a>



                    </div>
                @endforeach




            </div>

        </div>
    </div>
    <div class="factorysection carousel owl-carousel" data-carousel-items="1" data-carousel-dots="true">
  
        @foreach ($item as $items)
     
            <div class="factorycontent" style="    background-image: linear-gradient(
                rgba(0, 0, 0, 0.527),
                rgba(0, 0, 0, 0.5)
            ),
            url('{{ env('DATA_URL') }}/meuble/{{ $items->id }}.{{ $items->extension_image }}?v={{ $items->version }}');">
                <div class="title">{{ $items->label }}</div>
                <div class="text"> {{ $items->text }}</div>
                <div class="learnmore">
                    <a href="/liberiapackaging" target="_blank">{{ $items->button }}</a>
                </div>
            </div>
        @endforeach

    </div>
    <div class="contactus">
        <div class="contactuscontent">
            <div class="title">
                Contact us

            </div>
            <div class="contactusform">
                <form action="{{ route('contactusliberia') }}" method="POST">
                    @csrf

                    <div class="row">
                        <div class="inputone">
                            <div class="firstname">First Name <div class="star">*</div>
                            </div>

                            <input required type="text" placeholder="First Name" name="firstname">
                        </div>
                        <div class="inputtwo">
                            <div class="firstname">Last Name <div class="star">*</div>
                            </div>

                            <input required type="text"  id="" placeholder="Last Name" name="lastname">
                        </div>
                    </div>
                    <div class="row">

                        <div class="inputone">
                            <div class="firstname">Email address <div class="star">*</div>
                            </div>


                            <input required type="email" placeholder="Email address" name="emailaddress">
                        </div>
                        <div class="inputtwo">
                            <div class="firstname">Country <div class="star">*</div>
                            </div>
                            <select required class="nigeria33" name="country" id=""> <option value="">Liberia</option><option value="">Liberia</option></select>
                            {{-- <input class="nigeria33" type="text" placeholder="Liberia" value=""> --}}

                        </div>
                    </div>
                    <div class="row">
                        <div class="inputthree">
                            <div class="firstname">Message <div class="star">*</div>
                            </div>

                            <input name="message" required type="text" placeholder="Message">
                        </div>
                    </div>
                    <div class="submit">
                        <input  type="submit" name="" id="" value="Send Message">
                    </div>
                </form>
            </div>
        </div>
    </div>

    @component('footer.footer')
    @endcomponent

  
@endsection
