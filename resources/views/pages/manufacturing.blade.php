@extends('layouts.mainlayout') @section('title')
    <title> Nigeria Home Page </title>
@endsection
@extends('mainmenu.menutwo')
@section('content')
    <div class="right">
        <div class="socialmedia">
            <div class="youtube">
                <i class="fa-solid fa-store"></i>
            </div>
            <div class="facebook">
                <i class="fa-brands fa-facebook-f"></i>
            </div>

            <div class="instagram">
                <i class="fa-brands fa-instagram"></i>
            </div>

        </div>
    </div>
    <div class="manufacturing">
        @foreach ($manu as $slide)
            <div class="slide"
                style="background-image: linear-gradient(
                                                                                                    rgba(0, 0, 0, 0.527),
                                                                                                    rgba(0, 0, 0, 0.5)
                                                                                                ), url('{{ env('DATA_URL') }}/manufacturing/{{ $slide->id }}.{{ $slide->extension_image }}?v={{ $slide->version }}')">
                <div class="slidecontent">
                    <div class="title">{{ $slide->label }}</div>

                </div>
            </div>
        @endforeach
    </div>
    <div class="hometitlelg contant">
        <div class="home"> Home/</div>
        <div class="nigeria">Nigeria/</div>
        <div class="lg">Manufacturing</div>
    </div>
    <div class="factories">
        <div class="showroomcontent">
            <div class="title">Categories</div>
            <div class="aboutusnav">
                @foreach ($categories as $category)
                    @isset($category)
                        <button @if ($loop->first) data-id="brand-{{ $category->id }}" @endif
                            class="title-nav-1 @if ($loop->first) active @endif"
                            data-id="brand-{{ $category->id }}"
                            onclick="categories('brand-{{ $category->id }}')">{{ $category->label }}</button>
                    @endisset
                @endforeach



            </div>




            @foreach ($categories as $about)
                <div @if ($loop->first) id="brand-{{ $about->id }}" class="aboutusdetails_3" @endif
                    id="brand-{{ $about->id }}" class="aboutusdetails">

                    <div class="alltexts">

                        {{-- @foreach ($about as $card) --}}
                        {{-- @isset($card) --}}
                        <div class="abouttext">
                            {{ $about->textone }}.




                        </div>
                        <div class="abouttext-2">{{ $about->texttwo }}</div>
                        <div class="abouttext-3">
                            {{ $about->textthree }}

                        </div>
                        {{-- @endisset
                        @endforeach --}}

                    </div>




                    {{-- @foreach ($categories as $image) --}}
                    @php
                        $about = json_decode($about->gallery);
                    @endphp


                    <div class="aboutimage">
                        <a target="_blank" data-fancybox="gallery" href="{{ env('DATA_URL') }}/manucategories/{{ $about[0] }}"
                            class="small-image">
                            <div class="innerimage">
                                <img src="{{ env('DATA_URL') }}/manucategories/{{ $about[0] }}" alt="">

                            </div>
                        </a>
                        <div class="allsmallimages  carousel owl-carousel" data-carousel-items="6" data-carousel-nav="true">
                            @foreach ($about as $images)
                                <a target="_blank" data-fancybox="gallery"
                                    href="{{ env('DATA_URL') }}/manucategories/{{ $images }}" class="small-image">
                                    <div class="small-images">

                                        <img src="{{ env('DATA_URL') }}/manucategories/{{ $images }}" alt="">
                                    </div>
                                </a>
                            @endforeach
                        </div>



                    </div>
                    {{-- @endforeach --}}

                </div>
            @endforeach






        </div>
    </div>
    <script>
        function openCity(cityName) {
            var i;
            var x = document.getElementsByClassName("aboutusdetails");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            document.getElementById(cityName).style.display = "flex";
        }
    </script>

    @component('footer.footer')
    @endcomponent
@endsection
