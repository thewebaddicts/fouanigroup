@extends('layouts.mainlayout') @section('title')
    <title> Nigeria Home Page </title>
@endsection
@extends('mainmenu.congomenu')
@section('content')
    <div class="right">
        <div class="socialmedia">
            <div class="youtube">
                <i class="fa-solid fa-store"></i>
            </div>
            <div class="facebook">
                <i class="fa-brands fa-facebook-f"></i>
            </div>

            <div class="instagram">
                <i class="fa-brands fa-instagram"></i>
            </div>

        </div>
    </div>
    <div class="lgbrands">
        @foreach ($brands as $brand)
            <div class="slide"
                style="   background-image: linear-gradient(
                                rgba(0, 0, 0, 0.527),
                                rgba(0, 0, 0, 0.5)
                            ),
                            url('{{ env('DATA_URL') }}/congoslideshowimage/{{ $brand->id }}.{{ $brand->extension_slideshowimage }}?v={{ $brand->version }}');">
                <div class="slidecontent">
                    <div class="title">{{ $brand->slideshowlabel }}</div>
                    <a href="{{ $brand->slideshowbuttonlink }}">
                        <div class="store">
                         {{ $brand->slideshowbutton }}
                        </div>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
    <div class="hometitlelg contant">
        <div class="home"> Home/</div>
        <div class="nigeria">Liberia/</div>
        @foreach ($brands as $brand)
            <div class="lg">{{ $brand->brandname }}</div>
        @endforeach
    </div>

    <div class="lgelectronics">
        <div class="lgelectronicscontent">
            @foreach ($brands as $electronics)
                <div class="container">
                    <div class="title">{{ $electronics->electronicbrandlabel }}</div>
                    <div class="alltexts">
                        <div class="text-1"> {{ $electronics->textone }}
                        </div>
                        <div class="text-2"> {{ $electronics->texttwo }}
                        </div>
                    </div>
                    <div class="buttons">
                        <a href="{{ $electronics->buttononelink }}" target="_blank">{{ $electronics->buttonone }}</a>
                        <a href="{{ $electronics->buttontwolink}}" target="_blank">{{ $electronics->buttontwo }}</a>
                        <a href="{{ $electronics->buttonthreelink }}"
                            target="_blank">{{ $electronics->buttonthree }}</a>
                    </div>
                </div>




                <div class="lgimage">
                    <img src="{{ env('DATA_URL') }}/congoelecbrand/{{ $electronics->id }}.{{ $electronics->extension_electronicbrandimage }}?v={{ $electronics->version }}"
                        alt="">
                </div>
            @endforeach
        </div>
    </div>

    <div class="categories">
        <div class="title">Categories</div>
        <div class="allcards carousel owl-carousel " data-carousel-items="5" data-carousel-nav="true">
            @foreach ($brands as $cards)
                @php
                    $cards = json_decode($cards->categories);
                @endphp
                @foreach ($cards as $card)
                    <div class="card">
                        <div class="mask">
                            <div class="label">{{ $card->text }}</div>
                        </div>
                        <div class="card-image">
                            <img src="{{ env('DATA_URL') . $card->image }}" alt="">
                        </div>
                    </div>
                @endforeach
            @endforeach


        </div>
    </div>






    @component('footer.footer')
    @endcomponent
@endsection
