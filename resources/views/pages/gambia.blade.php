@extends('layouts.mainlayout') @section('title')
    <title> Congo Home Page </title>
@endsection
@extends('mainmenu.gambiamenu')
@section('content')
    <div class="right">
        <div class="socialmedia">

            <div class="facebook">
                <i class="fa-brands fa-facebook-f"></i>
            </div>

            <div class="instagram">
                <i class="fa-brands fa-instagram"></i>
            </div>

        </div>
    </div>
    <div class="gambiahome">
        @foreach ($slideshow as $slide)
            <div class="slide"
                style="    background-image: linear-gradient(
                        rgba(0, 0, 0, 0.527),
                        rgba(0, 0, 0, 0.5)
                    ),
                    url('{{ env('DATA_URL') }}/gambiahomepage/{{ $slide->id }}.{{ $slide->extension_gambiaimage }}?v={{ $slide->version }}');">
                <div class="slidecontent">
                    <div class="title">{{ $slide->text }}</div>
                    <div class="store">
                        {{ $slide->button }}
                    </div>
                </div>

            </div>
        @endforeach

    </div>
    <div class="hometitlecongo contant">
        <div class="home"> Home/</div>
        <div class="nigeria">Gambia</div>

    </div>

    <div class="overview">
        <div class="overviewcontent">
            <div class="title">Overview</div>
            <div class="overview2">
                @foreach ($overview as $over)
                    <div class="overviewimage">

                        <img src="{{ env('DATA_URL') }}/liberiaoverview/{{ $over->id }}.{{ $over->extension_image }}?v={{ $slide->version }}"
                            alt="">
                    </div>
                    <div class="overviewtext">
                        <div class="overviewtitle">{{ $over->label }}</div>
                        <div class="text">
                            <div class="textone"> {{ $over->textone }}
                            </div>
                            <div class="texttwo"> {{ $over->texttwo }}</div>
                            <div class="textthree">
                                {{ $over->textthree }}
                            </div>
                        </div>

                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="electronicsbrandsgambia">
        <div class="electronicscontent">
            <div class="title">Electronics Brands</div>
            <div class="text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                doloremque laudantium,
                totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
                explicabo. Nemo enim ipsam voluptatem quia voluptas.</div>

            <div class="allcard">
                @foreach ($brand as $brands)
                    <a href="/gambiaelectronicbrands/{{ $brands->id }}">
                        <div class="card">
                            <div class="cardimage">
                                <img src="{{ env('DATA_URL') }}/gambiaelectonicbrands/{{ $brands->id }}.{{ $brands->extension_cardimage }}?v={{ $brands->version }}"
                                    alt="">

                            </div>
                            <div class="cardimage2">
                                <img class="lg"
                                    src="{{ env('DATA_URL') }}/gambiaelectonicbrandss/{{ $brands->id }}.{{ $brands->extension_cardimage }}?v={{ $brands->version }}"
                                    alt="">
                            </div>
                        </div>
                    </a>
                @endforeach


            </div>

        </div>
    </div>
    <div class="fmcg">
        <div class="fmcgcontent">
            <div class="title">FMCG</div>
            <div class="text">
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
                explicabo. Nemo enim ipsam voluptatem quia voluptas.
            </div>
            <div class="allcards carousel owl-carousel" data-carousel-items="2">
                @foreach ($fmcg as $card)
                    <div class="card">
                        <a href="/gambiafmcg/{{ $card->id }}">
                            <div class="cardimage">
                                <img src="{{ env('DATA_URL') }}/gambiaslideshowfmcg/{{ $card->id }}.{{ $card->extension_coverimage }}?v={{ $card->version }}"
                                    alt="">
                            </div>
                        </a>



                    </div>
                @endforeach




            </div>

        </div>
    </div>

    <div class="contactus">
        <div class="contactuscontent">
            <div class="title">
                Contact

            </div>
            <div class="contactusform">
                <form action="{{ route('contactusgambia') }}" method="POST">
                    @csrf

                    <div class="row">
                        <div class="inputone">
                            <div class="firstname">Prénom <div class="star">*</div>
                            </div>

                            <input required name="firstname" type="text" placeholder="First Name">
                        </div>
                        <div class="inputtwo">
                            <div class="firstname">Nom <div class="star">*</div>
                            </div>

                            <input required name="lastname" type="text" name="" id="" placeholder="Last Name">
                        </div>
                    </div>
                    <div class="row">

                        <div class="inputone">
                            Adresse e-mail

                            <input required name="emailaddress" type="email" placeholder="Email address">
                        </div>
                        <div class="inputtwo">
                            Pays
                            {{-- <input required name="gambia" type="text" class="nigeria33" placeholder="gambia"> --}}
                            <select required name="country" class="nigeria33" id="">
                                <option value="">Gambia</option>
                                <option value="">Gambia</option>
                            </select>


                        </div>
                    </div>
                    <div class="row">
                        <div class="inputthree">
                            Message
                            <input required name="message" type="text" placeholder="Message">
                        </div>
                    </div>
                    <div class="submit">
                        <input type="submit" name="" id="" value="Send Message">
                    </div>
                </form>
            </div>
        </div>
    </div>

    @component('footer.footer')
    @endcomponent
    <script>

    </script>
