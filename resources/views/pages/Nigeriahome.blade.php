@extends('layouts.mainlayout') @section('title')
    <title> Nigeria Home Page </title>
@endsection
@extends('mainmenu.menutwo')
@section('content')
    <div class="right">
        <div class="socialmedia">
            <div class="youtube">
                <i class="fa-solid fa-store"></i>
            </div>
            <div class="facebook">
                <i class="fa-brands fa-facebook-f"></i>
            </div>

            <div class="instagram">
                <i class="fa-brands fa-instagram"></i>
            </div>

        </div>
    </div>

    <div class="Nigeriahomepage">

        @foreach ($slideshow as $slide)
            <div class="slide"
                style="     background-image: linear-gradient(
                                            rgba(0, 0, 0, 0.527),
                                            rgba(0, 0, 0, 0.5)
                                        ),
                                        url('{{ env('DATA_URL') }}/Nigeria/{{ $slide->id }}.{{ $slide->extension_image }}?v={{ $slide->version }}');">
                <div class="slidecontent">
                    <div class="title"> {{ $slide->label }}</div>
                    <div class="store">
                        {{ $slide->buttontext }}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="hometitle contant">
        <div class="home"> Home/</div>
        <div class="nigeria">Nigeria</div>
    </div>
    <div class="overview">
        <div class="overviewcontent">
            <div class="title">Overview</div>

            <div class="overview2">
                @foreach ($overview as $view)
                    <div class="overviewimage">
                        <img src="{{ env('DATA_URL') }}/overview/{{ $view->id }}.{{ $view->extension_image }}?v={{ $view->version }}"
                            alt="">
                    </div>
                    <div class="overviewtext">

                        <div class="overviewtitle">{{ $view->label }}</div>
                        <div class="text">
                            <div class="textone"> {{ $view->textone }}
                            </div>
                            <div class="texttwo">{{ $view->texttwo }}</div>
                            <div class="textthree">
                                {{ $view->textthree }}
                            </div>
                        </div>


                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="electronicsbrands">
        <div class="electronicscontent">
            <div class="title">Electronics Brands</div>
            <div class="text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                doloremque laudantium,
                totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
                explicabo. Nemo enim ipsam voluptatem quia voluptas.</div>
            <div class="allcard carousel owl-carousel" data-carousel-items="2">
                @foreach ($brands as $brand)
                    <a href="brand/{{ $brand->id }}">
                        <div class="card " >
                            <div class="cardimage">
                                <img src="{{ env('DATA_URL') }}/bradimage/{{ $brand->id }}.{{ $brand->extension_brandimage }}?v={{ $brand->version }}"
                                    alt="">

                            </div>
                            <div class="cardimage2">
                                <img class="lg"
                                    src="{{ env('DATA_URL') }}/brandimageonhover/{{ $brand->id }}.{{ $brand->extension_brandonhover }}?v={{ $brand->version }}"
                                    alt="">
                            </div>
                        </div>
                    </a>
                @endforeach


            </div>

        </div>
    </div>
    <div class="outlets">
        <div class="outletscontent">
            <div class="title">
                Outlets

            </div>
            <div class="text">
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi.
            </div>
            <div class="allcards">
                @foreach ($outlits as $outlit)
                    <div class="card">
                        <div class="card-image"><img
                                src="{{ env('DATA_URL') }}/showroom/{{ $outlit->id }}.{{ $outlit->extension_showroomimage }}?v={{ $outlit->version }}"
                                alt=""></div>
                        <div class="label">
                            {{ $outlit->showroomlabel }}

                        </div>
                        <div class="showmore">
                            <a href="/outlet/{{ $outlit->id }}">View More

                            </a>
                        </div>
                    </div>
                @endforeach


            </div>
        </div>
    </div>
    <div class="fmcg">
        <div class="fmcgcontent">
            <div class="title">FMCG</div>
            <div class="text">
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
                explicabo. Nemo enim ipsam voluptatem quia voluptas.
            </div>
            <div class="allcards carousel owl-carousel" data-carousel-items="2">
                @foreach ($fmcg as $card)
                    <div class="card">
                        <a href="/fmcg/{{ $card->id }}">
                            <div class="cardimage">
                                <img src="{{ env('DATA_URL') }}/fmcg/{{ $card->id }}.{{ $card->extension_coverimage }}?v={{ $card->version }}"
                                    alt="">
                            </div>
                        </a>



                    </div>
                @endforeach




            </div>

        </div>
    </div>
    <div class="factorysection carousel owl-carousel" data-carousel-items="1" data-carousel-dots="true">
        @foreach ($factory as $factories)
            <div class="factorycontent" style="    background-image: linear-gradient(
                rgba(0, 0, 0, 0.527),
                rgba(0, 0, 0, 0.5)
            ),
            url('{{ env('DATA_URL') }}/meuble/{{ $factories->id }}.{{ $factories->extension_image }}?v={{ $factories->version }}');">
                <div class="title">{{ $factories->slideshow }}</div>
                <div class="text">{{ $factories->text }}</div>
                <div class="learnmore">
                    <a href="/nigeriafactory" target="_blank">{{ $factories->button }}</a>
                </div>
            </div>
        @endforeach

    </div>
    <div class="contactus">
        <div class="contactuscontent">
            <div class="title">
                Contact us

            </div>
            <div class="contactusform">
                <form action="{{ route('contactusnigera') }}" method="POST">
                    @csrf

                    <div class="row">
                        <div class="inputone">
                            <div class="firstname">First Name <div class="star">*</div>
                            </div>
                            <input type="text" placeholder="First Name" name="firstname">
                        </div>
                        <div class="inputtwo">
                            <div class="firstname"> Last Name <div class="star">*</div>
                            </div>
                            <input type="text" name="" id="" placeholder="Last Name" name="lastname">
                        </div>
                    </div>
                    <div class="row">

                        <div class="inputone">
                            <div class="firstname"> Email address <div class="star">*</div>
                            </div>

                            <input type="email" placeholder="Email address" name="emailaddress">
                        </div>
                        <div class="inputtwo">
                            Country
                            <select name="country" class="nigeria33" type="text" placeholder="Nigeria" value="Nigeria"> 
                                <option value="Nigeria">Nigeria </option>
                            </select>


                        </div>
                    </div>
                    <div class="row">
                        <div class="inputthree">
                            Message
                            <input type="text" placeholder="Message">
                        </div>
                    </div>
                    <div class="submit">
                        <input type="submit" name="" id="" value="Send Message" name="message">
                    </div>
                </form>
            </div>
        </div>
    </div>

    @component('footer.footer')
    @endcomponent

    {{-- <script>
        document.querySelector('.select-wrapper').addEventListener('click', function() {
            this.querySelector('.select').classList.toggle('open');
        })
    </script> --}}
@endsection
