@extends('layouts.mainlayout') @section('title')
    <title> Lebanon Home Page </title>
@endsection
@extends('mainmenu.lebanonmenu')
@section('content')
    <div class="right">
        <div class="socialmedia">

            <div class="facebook">
                <i class="fa-brands fa-facebook-f"></i>
            </div>

            <div class="instagram">
                <i class="fa-brands fa-instagram"></i>
            </div>

        </div>
    </div>
    <div class="lebanonhome">
        @foreach ($slideshow as $slide)
            <div class="slide" style="    background-image: linear-gradient(
                rgba(0, 0, 0, 0.527),
                rgba(0, 0, 0, 0.5)
            ),
            url('{{ env('DATA_URL') }}/lebanonhompage/{{ $slide->id }}.{{ $slide->extension_slideshow }}?v={{ $slide->version }}');">
                <div class="slidecontent">
                    <div class="title">{{ $slide->label }}</div>
                    <div class="store">
                        {{ $slide->button }}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="hometitlecongo contant">
        <div class="home"> Home/</div>
        <div class="nigeria">Lebanon</div>

    </div>

    <div class="overview">
        <div class="overviewcontent">
            <div class="title">Overview</div>
            <div class="overview2">
                @foreach ($overview as $over)
                    <div class="overviewimage">

                        <img src="{{ env('DATA_URL') }}/lebanonoverview/{{ $over->id }}.{{ $over->extension_image }}?v={{ $slide->version }}"
                            alt="">
                    </div>
                    <div class="overviewtext">
                        <div class="overviewtitle">{{ $over->label }}</div>
                        <div class="text">
                            <div class="textone"> {{ $over->textone }}
                            </div>
                            <div class="texttwo"> {{ $over->texttwo }}</div>
                            <div class="textthree">
                                {{ $over->textthree }}
                            </div>
                        </div>

                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="realstate carousel owl-carousel" data-carousel-items="1" data-carousel-dots="true">
        @foreach ($item as $items)
     
        <div class="factorycontent" style="   background-image: linear-gradient(
            rgba(0, 0, 0, 0.527),
            rgba(0, 0, 0, 0.5)
        ),
        url('{{ env('DATA_URL') }}/lebanonrealstate/{{ $slide->id }}.{{ $slide->extension_slideshow }}?v={{ $slide->version }}');">
            <div class="title">{{ $items->label }}</div>
            <div class="text"> {{ $items->text }}</div>
            <div class="learnmore">
                <a href="{{ $items->buttonlink }}" target="_blank">{{ $items->button }}</a>
            </div>
        </div>
    @endforeach
      
    </div>


    <div class="contactus">
        <div class="contactuscontent">
            <div class="title">
                Contact us

            </div>
            <div class="contactusform">
                <form action="{{ route('contactuslebanon') }}" method="POST">
                    @csrf

                    <div class="row">
                        <div class="inputone">
                            <div class="firstname">First Name <div class="star">*</div>
                            </div>

                            <input name="firstname" type="text" placeholder="First Name">
                        </div>
                        <div class="inputtwo">
                            <div class="firstname">Last Name <div class="star">*</div>
                            </div>

                            <input name="lastname" type="text" name="" id="" placeholder="Last Name">
                        </div>
                    </div>
                    <div class="row">

                        <div class="inputone">
                            <div class="firstname">Email address <div class="star">*</div>
                            </div>


                            <input name="emailaddress" type="email" placeholder="Email address">
                        </div>
                        <div class="inputtwo">
                            Country
                            <select name="country" id=""><option value="">Lebanon</option> <option value="">Lebanon</option></select>


                        </div>
                    </div>
                    <div class="row">
                        <div class="inputthree">
                            Message
                            <input type="text" placeholder="Message">
                        </div>
                    </div>
                    <div class="submit">
                        <input type="submit" name="" id="" value="Send Message">
                    </div>
                </form>
            </div>
        </div>
    </div>

    @component('footer.footer')
    @endcomponent
    <script>

    </script>
