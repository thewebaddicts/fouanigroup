@extends('layouts.mainlayout') @section('title')
    <title> Nigeria Home Page </title>
@endsection
@extends('mainmenu.guinea')
@section('content')
    <div class="right">
        <div class="socialmedia">
            <div class="youtube">
                <i class="fa-solid fa-store"></i>
            </div>
            <div class="facebook">
                <i class="fa-brands fa-facebook-f"></i>
            </div>

            <div class="instagram">
                <i class="fa-brands fa-instagram"></i>
            </div>

        </div>
    </div>
    <div class="nutrimental">
        @foreach ($fmcg as $slide)
            <div class="slide"
                style="   flex-direction: column;
                        background-image: linear-gradient(
                                rgba(0, 0, 0, 0.527),
                                rgba(0, 0, 0, 0.5)
                            ),
                            url('{{ env('DATA_URL') }}/gambiaslideshowfmcg/{{ $slide->id }}.{{ $slide->extension_gambiaslideshow }}?v={{ $slide->version }}');">
                <div class="slidecontent">
                    <div class="title">{{ $slide->label }}</div>

                </div>
            </div>
        @endforeach
    </div>
    <div class="hometitlelg contant">
        <div class="home"> Home/</div>
        <div class="nigeria">Nigeria/</div>
        @foreach ($fmcg as $title)
            <div class="lg">{{ $title->label }}</div>
        @endforeach
    </div>
    <div class="nutriabout">
        <div class="nutricontent">
            @foreach ($fmcg as $cards)
            @php
                $cards=json_decode($cards->fmcg)
            @endphp
                @foreach ($cards as $card)
                    <div class="alltext">
                        <div class="title">{{ $card->title }}</div>
                        <div class="textone">{{ $card->textone }}</div>
                        <div class="textone">{{ $card->texttwo }}
                        </div>
                        <div class="semititle-1">{{ $card->subtitleone }}
                        </div>
                        <div class="textone"> {{ $card->textthree }}
                        </div>
                        <div class="semititle-1">{{ $card->subtitletwo }}
                        </div>
                        <div class="textone"> {{ $card->textfour }}</div>
                    </div>
                    <div class="n-image">
                        <img src="{{ env('DATA_URL') . $card->image }}" alt="">

                    </div>
                @endforeach
            @endforeach
        </div>
    </div>
    @component('footer.footer')
    @endcomponent
@endsection
