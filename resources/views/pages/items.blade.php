@extends('layouts.mainlayout') @section('title')
    <title> Lebanon Home Page </title>
@endsection
@extends('mainmenu.mainthree')
@section('content')
    <div class="right">
        <div class="socialmedia">
            <div class="youtube">
                <i class="fa-solid fa-store"></i>
            </div>
            <div class="facebook">
                <i class="fa-brands fa-facebook-f"></i>
            </div>

            <div class="instagram">
                <i class="fa-brands fa-instagram"></i>
            </div>

        </div>
    </div>
    <div style="padding-top:120px">

        <div class="hometitlebanon contant">
            <div class="home"> Home/</div>
            <div class="nigeria">Nigeria/</div>
            <div class="service">Service Request/</div>
            <div class="real">In-home Service</div>

        </div>

        <div class="rednavbar2">
            <div class="allcircles">
                <div class="card">
                    <div class="circle">
                        1

                    </div>
                    <div class="text1">
                        Brand & Category
                    </div>
                </div>
                <div class="card">
                    <div class="circle-2">
                        2

                    </div>
                    <div class="text2">
                        Item Details
                    </div>
                </div>
                <div class="card">

                    <div class="circle-3">
                        3

                    </div>
                    <div class="text3">
                        Error Details
                    </div>
                </div>
                <div class="card">
                    <div class="circle-4">
                        4

                    </div>
                    <div class="text4">
                        Personal Information
                    </div>
                </div>

                <div class="card">
                    <div class="circle-5">
                        5

                    </div>
                    <div class="text5">
                        Request Summary
                    </div>
                </div>




            </div>

        </div>

        <form action="{{ route('itemsdetails') }}" method="POST">
            @csrf
            <div class="items">
                <div class="title">Fill Item Details</div>
                <div class="card">
                    <div class="form">



                        <div class="row">
                            <div class="title">
                                <div class="firstname">Item Sub-category <div class="star">*</div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="i-1"><select required name="item" id="">
                                        <option value="itemssubcategory">Item Sub-category</option>
                                        <option value="itemssubcategory">Item Sub-category</option>
                                    </select></div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="container">
                                <div class="i-3">
                                    <div class="title">Size</div><input required name="size" placeholder="Size" type="text"
                                        name="" id="">
                                </div>
                                <div class="i-3">
                                    <div class="title">Model Number</div><input required name="model"
                                        placeholder="Model Number" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="container">
                                <div class="i-3">
                                    <div class="title">Serial Number</div><input required name="serialnumber"
                                        placeholder="Serial Number" type="text" name="" id="">
                                </div>
                                <div class="i-3">
                                    <div class="title">Purchase Date</div><input required name="purchasedate"
                                        placeholder="Purchase Date" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="title">Serial Number</div>

                            <div class="container">

                                <div class="insidecontainer">

                                    <div class="i-5">
                                        <div class="up"><svg xmlns="http://www.w3.org/2000/svg" width="29.789"
                                                height="36.172" viewBox="0 0 29.789 36.172">
                                                <path id="Icon_material-file-upload" data-name="Icon material-file-upload"
                                                    d="M16.011,32.161H28.778V19.395h8.511L22.395,4.5,7.5,19.395h8.511ZM7.5,36.417H37.289v4.256H7.5Z"
                                                    transform="translate(-7.5 -4.5)" fill="#bd1622" />
                                            </svg>
                                        </div>
                                        <div class="drop">Drag and Drop here or <div class="browse">Browse
                                                files</div>
                                        </div>
                                        <label class="lab">
                                            Upload
                                            <input required name="file" class="file" type="file" name="" id="" title=" ">


                                        </label>


                                    </div>
                                </div>



                            </div>
                        </div>









                    </div>

                </div>
            </div>


            <div class="submiting">
                <div> <i style="    position: relative;
                        left: 18px;
                        top: 2px; color:#e51240" class="fa-solid fa-chevron-left"></i> <input class="ps"
                        type="submit" name="" id="" value="Previous Step"></div>
                <div><input class="ns" type="submit" name="" id="" value="Next Step"><i style="    position: relative;
                        right: 24px;
               
                        top: 2px;
                        color: white;" class="fa-solid fa-chevron-right"></i></div>
            </div>

    </div>
    </form>


    @component('footer.footer')
    @endcomponent
@endsection
