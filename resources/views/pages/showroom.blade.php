@extends('layouts.mainlayout') @section('title')
    <title> Nigeria Home Page </title>
@endsection
@extends('mainmenu.menutwo')
@section('content')
    <div class="right">
        <div class="socialmedia">
            <div class="youtube">
                <i class="fa-solid fa-store"></i>
            </div>
            <div class="facebook">
                <i class="fa-brands fa-facebook-f"></i>
            </div>

            <div class="instagram">
                <i class="fa-brands fa-instagram"></i>
            </div>

        </div>
    </div>
    <div class="showroom">
        @foreach ($outlits as $slide)
            <div class="slide"
                style="   background-image: linear-gradient(
                                                                    rgba(0, 0, 0, 0.527),
                                                                    rgba(0, 0, 0, 0.5)
                                                                ),
                                                                url('{{ env('DATA_URL') }}/showrooms/{{ $slide->id }}.{{ $slide->extension_slideshowimage }}?v={{ $slide->version }}');">
                <div class="slidecontent">
                    <div class="title">{{ $slide->slideshowlabel }}</div>

                </div>
            </div>
        @endforeach
    </div>
    <div class="hometitlelg contant">
        <div class="home"> Home/</div>
        <div class="nigeria">Nigeria/</div>
        @foreach ($outlits as $title)
            <div class="lg">{{ $title->label }}</div>
        @endforeach
    </div>
    <div class="showroomcategories">
        <div class="showroomcontent">
            <div class="title">Categories</div>
            <div class="aboutusnav">
                @foreach ($categories as $brands)
                    <button @if ($loop->first) data-id="showroom-{{ $brands->id }}" @endif
                        data-id="showroom-{{ $brands->id }}" onclick="categories('showroom-{{ $brands->id }}')"
                        class="title-nav-1 @if ($loop->first) active @endif">{{ $brands->label }}</button>
                @endforeach



            </div>



            @foreach ($categories as $card)
                <div @if ($loop->first) id="showroom-{{ $card->id }} " class="aboutusdetails_4" @endif
                    id="showroom-{{ $card->id }}" class="aboutusdetails">




                    <div class="alltexts">

                        <div class="abouttext">
                            {{ $card->textone }}





                        </div>
                        <div class="abouttext-2"> {{ $card->texttwo }}</div>
                        <div class="abouttext-3">
                            {{ $card->textthree }}

                        </div>

                    </div>






                    <div class="aboutimage">
                        <div class="innerimage">
                            <img src="{{ env('DATA_URL') }}/showroomcategories/{{ $card->id }}.{{ $card->extension_image }}?v={{ $card->version }}"
                                alt="">

                        </div>

                    </div>

                </div>
            @endforeach




        </div>
    </div>
    <div class="location">
        <div class="title">
            <div class="titleloc">
                Locate us
            </div>


        </div>
        <div class="locationcontent contant">

            <div class="filterstitle"></div>
            <div class="filters">
                <div class="allinputs">
                    <div class="i-1"> <input id="inputString" placeholder="Enter city, town or state"
                            class="input1" type="text" name="" id=""> <img src="/images/search.png" alt=""></div>
                    <div class="i-2"> <select class="input2" name="" id="">
                            <option value="">Filter by State</option>
                        </select> </div>
                    <div class="i-3"> <select class="input3" name="" id="">
                            <option value="">Filter by Brand & Category</option>
                        </select> </div>
                </div>
                <div class="showlocation">
                    <i class="fa-solid fa-location-crosshairs"></i> Show Nearby Locations
                </div>


            </div>
            <div class="map">
                <div class="mapcontent">
                    @foreach ($map as $maps)
                        <div class="accordion">
                            <div class="accordion-item">

                                <div class="list">


                                    <div class="list_1 accordion-title">
                                        <div class="text">
                                            <b>{{ $maps->cityname }}</b>,{{ $maps->brandname }}
                                        </div> <i class="fa-solid fa-plus plus"></i> <i class="fa-solid fa-minus minus"></i>
                                    </div>
                                    <div class="km">{{ $maps->km }}</div>
                                    <div class="container accordion-content">
                                        <div class="locimage">
                                            <i class="fa-solid fa-location-dot"></i>
                                            <div class="texts">
                                                {{ $maps->location }}

                                            </div>
                                        </div>
                                        <div class="mapphone">
                                            <i class="fa-solid fa-phone"></i>
                                            <div class="phonetext">
                                                {{ $maps->phone }}
                                            </div>

                                        </div>
                                        <div class="mapemail">
                                            <i class="fa-solid fa-envelope"></i>
                                            <div class="emailtext">
                                                {{ $maps->mail }}
                                            </div>
                                        </div>
                                        <div class="mapedate">
                                            <i class="fa-solid fa-clock"></i>

                                            <div class="emailtext">
                                                {{ $maps->clock }}
                                            </div>
                                        </div>

                                        <div class="directions">
                                            Get Directions
                                        </div>
                                        @php
                                            $maps = json_decode($maps->images);
                                        @endphp

                                        <div class="directionimage">
                                            @foreach ($maps as $image)
                                                <a target="_blank" data-fancybox="gallery"
                                                    href="{{ env('DATA_URL') . $image->iamge }}"
                                                    class="small-image">
                                                    <img src="{{ env('DATA_URL') . $image->iamge }}" alt="">
                                                </a>
                                            @endforeach


                                        </div>




                                    </div>



                                </div>

                            </div>
                        </div>
                    @endforeach

                </div>
                <div class="fullmap">
                    <div class="mapContent maps" id="map"></div>

                </div>
            </div>
        </div>
    </div>
    @component('footer.footer')
    @endcomponent

    <script>
        var MarkersArrayElems = [];

        function initMap() {

            var settings = {
                center: new google.maps.LatLng(44, -110),
                zoom: 15
            };

            //console.log(settings);
            var map = new google.maps.Map(document.getElementById('map'), settings);

            var bounds = new google.maps.LatLngBounds();

            //   console.log(bounds);
            var infowindow = new google.maps.InfoWindow({
                maxWidth: 300
            });

            var MarkersArray = [];

            <?php
    foreach ($map as $boutique) {
    ?>
            MarkersArray.push(['<?php echo $boutique->lat; ?>', '<?php echo $boutique->long; ?>']);

            <?php } ?>
            console.log(MarkersArray);
            var zoom = map.getZoom();
            for (var i = 0; i < MarkersArray.length; i++) {

                var single_location = MarkersArray[i];
                var myLatLng = new google.maps.LatLng(single_location[0], single_location[1]);
                var marker = new google.maps.Marker({
                    position: myLatLng,
                    latlng: single_location[0] + ',' + single_location[1],
                    map: map,
                    title: single_location[3],
                    description: single_location[2],
                    gridid: single_location[4],
                });

                MarkersArrayElems[single_location[4]] = marker;
                bounds.extend(myLatLng);


                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.setContent('<div class="ContactMapTooltip"><h5 class=\'t-3 mt-0 mb-2 \'>' + this
                        .title + '</h5><span class=\'t-1 mt-0 mb-1 d-block\'>' + this.description +
                        '</span><br><a class="BackgroundGradient" target="_blank" href="https://www.google.com/maps/search/?api=1&query=' +
                        this.latlng + '">View in Google Maps</a></div>');
                    infowindow.open(map, this);
                    map.setZoom(15);
                    map.panTo(this.getPosition());
                });

            }

            google.maps.event.addListener(map, 'zoom_changed', function(ee) {
                ZoomLevel = this.getZoom();
                if (ZoomLevel == 0) {
                    map.setZoom(8);
                } else if (ZoomLevel > 15) {
                    map.setZoom(15);
                }
            });

            map.setCenter(bounds.getCenter());
            map.fitBounds(bounds);
            map.panToBounds(bounds);

        }

        // TO MAKE THE MAP APPEAR YOU MUST
        // ADD YOUR ACCESS TOKEN FROM
        // https://account.mapbox.com
        // mapboxgl.accessToken =
        //     'pk.eyJ1IjoibW9oYW1tZWRzaGF0aWxhIiwiYSI6ImNsMmltaHIzMjBoaXYzYnE2c2lpdm1sbnQifQ.c0F0A_xcEoR-I4HUrURRKg';
        // let map = new mapboxgl.Map({
        //     container: 'maps', // container ID
        //     style: 'mapbox://styles/mapbox/streets-v11', // style URL
        //     center: [9.0820, 8.6753], // starting position [lng, lat]
        //     zoom: 9 // starting zoom
        // });

        // let marker = new mapboxgl.Marker()
        //     .setLngLat([6.4646, 3.5725])
        //     .addTo(map);
        // let marker2 = new mapboxgl.Marker()
        //     .setLngLat([6.4553, 3.3641])
        //     .addTo(map);
        // let marker3 = new mapboxgl.Marker()
        //     .setLngLat([6.454592614539875, 3.3715293736430034])
        //     .addTo(map);
        // let marker4 = new mapboxgl.Marker()
        //     .setLngLat([6.6018, 3.3515])
        //     .addTo(map);
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=&callback=initMap" async defer>
    </script>
@endsection
