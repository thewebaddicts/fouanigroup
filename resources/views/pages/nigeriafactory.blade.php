@extends('layouts.mainlayout') @section('title')
    <title> Lebanon Home Page </title>
@endsection
@extends('mainmenu.menutwo')
@section('content')
    <div class="right">
        <div class="socialmedia">

            <div class="facebook">
                <i class="fa-brands fa-facebook-f"></i>
            </div>

            <div class="instagram">
                <i class="fa-brands fa-instagram"></i>
            </div>

        </div>
    </div>
    <div class="lebanonrealstate">
        @foreach ($slideshow as $slide)
            <div class="slide"
                style="   background-image: linear-gradient(
                                rgba(0, 0, 0, 0.527),
                                rgba(0, 0, 0, 0.5)
                            ),
                            url('{{ env('DATA_URL') }}/factory/{{ $slide->id }}.{{ $slide->extension_image }}?v={{ $slide->version }}');">
                <div class="slidecontent">
                    <div class="title">{{ $slide->slideshow }}</div>

                </div>
            </div>
        @endforeach

    </div>
    <div class="hometitlebanon contant">
        <div class="home"> Home/</div>
        <div class="nigeria">Lebanon</div>
        <div class="real">Factory</div>

    </div>
    @foreach ($slideshow as $about)
        <div id="" class="aboutusdetailss">

            <div class="alltexts">

                {{-- @foreach ($about as $card) --}}
                {{-- @isset($card) --}}
                <div class="abouttext">
                    {{ $about->textone }}.




                </div>
                <div class="abouttext-2">{{ $about->texttwo }}</div>
                <div class="abouttext-3">
                    {{ $about->textthree }}

                </div>
                {{-- @endisset
            @endforeach --}}

            </div>




            {{-- @foreach ($categories as $image) --}}
            @php
                $about = json_decode($about->gallery);
            @endphp


            <div class="aboutimage">
                <div class="innerimage">
                    <img src="{{ env('DATA_URL') }}/factorynigeriapage/{{ $about[0] }}" alt="">

                </div>
                <div class="allsmallimages">
                    @foreach ($about as $images)
                        <div class="small-images">

                            <img src="{{ env('DATA_URL') }}/factorynigeriapage/{{ $images }}" alt="">
                        </div>
                    @endforeach
                </div>



            </div>
            {{-- @endforeach --}}

        </div>
    @endforeach
    {{-- <div class="factories">
        <div class="showroomcontent">






            <div id="whoweare" class="aboutusdetails">
                <div class="alltexts">
                    <div class="abouttext">
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                        totam
                        rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
                        explicabo. Nemo enim ipsam voluptatem quia voluptas.




                    </div>
                    <div class="abouttext-2"> Ded quia consequuntur magni dolores eos qui ratione voluptatem sequi
                        nesciunt. Neque porro quisquam est,
                        qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi
                        tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam,
                        quis
                        nostrum exercitationem.</div>
                    <div class="abouttext-3">
                        Ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum
                        iure
                        reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem
                        eum
                        fugiat quo voluptas nulla pariatur? At vero eos et accusamus et iusto odio dignissimos ducimus qui
                        blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi
                        sint
                        occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id
                        est
                        laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.
                    </div>

                </div>





                <div class="aboutimage">
                    <div class="innerimage">
                        <img src="/images/fac.png" alt="">

                    </div>
                    <div style="display: flex;column-gap:16px">
                        <div class="small-images">

                            <img src="/images/dj.png" alt="">
                        </div>
                        <div class="small-images">

                            <img src="/images/dj.png" alt="">
                        </div>


                    </div>


                </div>
            </div>





        </div>
    </div> --}}

    <div class="overview" style="padding-bottom: 150px">
        <div class="overviewcontent">

            <div class="overview2">
                @foreach ($slideshow as $overview)
                    <div class="overviewimage">
                        <img src="{{ env('DATA_URL') }}/factimage/{{ $overview->id }}.{{ $overview->extension_image }}?v={{ $overview->version }}"
                            alt="">
                    </div>
                    <div class="overviewtext">



                        <div class="overviewtitle">{{ $overview->secondpartlabel }}</div>
                        @php
                            $overview = json_decode($overview->secondpart);
                        @endphp

                        @foreach ($overview as $view)
                            <div class="text">

                                <div class="textone">{{ $view->secondparttext }}
                                </div>


                            </div>
                        @endforeach


                    </div>
                @endforeach
            </div>
        </div>
    </div>





    @component('footer.footer')
    @endcomponent
    <script>

    </script>
