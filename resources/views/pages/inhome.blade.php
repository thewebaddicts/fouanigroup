@extends('layouts.mainlayout') @section('title')
    <title> Lebanon Home Page </title>
@endsection
@extends('mainmenu.mainthree')
@section('content')
<div class="right">
    <div class="socialmedia">
        <div class="youtube">
            <i class="fa-solid fa-store"></i>
        </div>
        <div class="facebook">
            <i class="fa-brands fa-facebook-f"></i>
        </div>
      
        <div class="instagram">
            <i class="fa-brands fa-instagram"></i>
        </div>
    
    </div>
</div>
    <div style="padding-top:120px">

        <div class="hometitlebanon contant">
            <div class="home"> Home/</div>
            <div class="nigeria">Nigeria/</div>
            <div class="service">Service Request/</div>
            <div class="real">In-home Service</div>

        </div>

        <div class="rednavbar">
            <div class="allcircles">
                <div class="card">
                    <div class="circle">
                        1
    
                    </div>
                    <div class="text1">
                        Brand & Category
                    </div>
                </div>
                <div class="card">
                    <div class="circle-2">
                        2
    
                    </div>
                    <div class="text2">
                        Item Details
                    </div>
                </div>
                <div class="card">
                    
                <div class="circle-3">
                    3

                </div>
                <div class="text3">
                    Error Details
                </div>
                </div>
                <div class="card">
                    <div class="circle-4">
                        4
    
                    </div>
                    <div class="text4">
                        Personal Information
                    </div>
                </div>

                <div class="card">
                    <div class="circle-5">
                        5
    
                    </div>
                    <div class="text5">
                        Request Summary
                    </div>
                </div>
             
             
             
             
            </div>
          
        </div>
        <div class="inhometext">
            <div class="text">
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. 
            </div>
        </div>
        <div class="brands">
            <div class="allbrands">
                <div class="title">Select Brand</div>
                <div class="allcards">
                    <div class="card">
                        <img src="/images/lg.png" alt="">
                    </div>
                    <div class="card">
                        <img src="/images/lg.png" alt="">
                    </div>
                    <div class="card">
                        <img src="/images/lg.png" alt="">
                    </div>
                </div>
            </div>
        </div>

      
    </div>
    @component('footer.footer')
        
    @endcomponent
@endsection
