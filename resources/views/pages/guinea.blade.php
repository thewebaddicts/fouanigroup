@extends('layouts.mainlayout') @section('title')
    <title> Congo Home Page </title>
@endsection
@extends('mainmenu.guinea')
@section('content')
    <div class="right">
        <div class="socialmedia">

            <div class="facebook">
                <i class="fa-brands fa-facebook-f"></i>
            </div>

            <div class="instagram">
                <i class="fa-brands fa-instagram"></i>
            </div>

        </div>
    </div>
    <div class="congohome">
        @foreach ($slideshow as $slide)
            <div class="slide">
                <div class="slidecontent" style="background-image: linear-gradient(
                    rgba(0, 0, 0, 0.527),
                    rgba(0, 0, 0, 0.5)
                ), url('{{ env('DATA_URL') }}/guineahomepage/{{ $slide->id }}.{{ $slide->extension_slideshowimage }}?v={{ $slide->version }}">
                    <div class="title">{{ $slide->label }}</div>
                    <div class="store">
                        {{ $slide->button }}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="hometitlecongo contant">
        <div class="home"> Accueil/</div>
        <div class="nigeria">Congo</div>

    </div>

    <div class="overview">
        <div class="overviewcontent">
            <div class="title">Aperçu</div>
            <div class="overview2">
                @foreach ($overview as $over)
                    <div class="overviewimage">

                        <img src="{{ env('DATA_URL') }}/liberiaoverview/{{ $over->id }}.{{ $over->extension_image }}?v={{ $slide->version }}"
                            alt="">
                    </div>
                    <div class="overviewtext">
                        <div class="overviewtitle">{{ $over->label }}</div>
                        <div class="text">
                            <div class="textone"> {{ $over->textone }}
                            </div>
                            <div class="texttwo"> {{ $over->texttwo }}</div>
                            <div class="textthree">
                                {{ $over->textthree }}
                            </div>
                        </div>

                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="electronicsbrandsliberia">
        <div class="electronicscontent">
            <div class="title">Electronics Brands</div>
            <div class="text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                doloremque laudantium,
                totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
                explicabo. Nemo enim ipsam voluptatem quia voluptas.</div>
            <div class="location">
                <div class="loc-1"><i class="fa-solid fa-location-dot"></i></div>
                <div class="text-2">Showroom Located in
                    City, Country Floor, Building, Street</div>
            </div>
            <div class="allcard">
                @foreach ($brand as $brands)
                    <a href="/guineaelectronicbrands/{{ $brands->id }}">
                        <div class="card">
                            <div class="cardimage">
                                <img src="{{ env('DATA_URL') }}/cardimage/{{ $brands->id }}.{{ $brands->extension_cardimage }}?v={{ $brands->version }}"
                                    alt="">

                            </div>
                            <div class="cardimage2">
                                <img class="lg"
                                    src="{{ env('DATA_URL') }}/guineaelectronics/{{ $brands->id }}.{{ $brands->extension_hoverimage }}?v={{ $brands->version }}"
                                    alt="">
                            </div>
                        </div>
                    </a>
                @endforeach


            </div>

        </div>
    </div>
    <div class="fmcg">
        <div class="fmcgcontent">
            <div class="title">FMCG</div>
            <div class="text">
                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
                explicabo. Nemo enim ipsam voluptatem quia voluptas.
            </div>
            <div class="allcards carousel owl-carousel" data-carousel-items="2">
                @foreach ($fmcg as $card)
                    <div class="card">
                        <a href="/guineafmcg/{{ $card->id }}">
                            <div class="cardimage">
                                <img src="{{ env('DATA_URL') }}/guineacoverimage/{{ $card->id }}.{{ $card->extension_coverimage }}?v={{ $card->version }}"
                                    alt="">
                            </div>
                        </a>



                    </div>
                @endforeach




            </div>

        </div>
    </div>
    <div class="factorysection carousel owl-carousel" data-carousel-items="1" data-carousel-dots="true">
        @foreach ($factory as $factories)
            <div class="factorycontent" style="    background-image: linear-gradient(
                rgba(0, 0, 0, 0.527),
                rgba(0, 0, 0, 0.5)
            ),
            url('{{ env('DATA_URL') }}/meuble/{{ $factories->id }}.{{ $factories->extension_image }}?v={{ $factories->version }}');">
                <div class="title">{{ $factories->label }}</div>
                <div class="text">{{ $factories->text }}</div>
                <div class="learnmore">
                    <a  target="_blank" href="/guineaimmobilier">{{ $factories->button }}</a>
                </div>
            </div>
        @endforeach

    </div>
    {{-- <div class="factorysection carousel owl-carousel" data-carousel-items="1" data-carousel-dots="true">
        <div class="factorycontent">
            <div class="title">Immobilier 1</div>
            <div class="text">Ullam corporis suscipit laboriosam nisi ut aliquid ex ea commodi conatur.
                Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam
                nihil molestiae consequatur, vel illum qui dolorem eum.</div>
            <div class="learnmore">
                <a href="">En Savoir Plus</a>
            </div>
        </div>
        <div class="factorycontent">
            <div class="title">Immobilier 2</div>
            <div class="text">Ullam corporis suscipit laboriosam nisi ut aliquid ex ea commodi conatur.
                Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam
                nihil molestiae consequatur, vel illum qui dolorem eum.</div>
            <div class="learnmore">
                <a href="">En Savoir Plus</a>
            </div>
        </div>
    </div> --}}
    <div class="contactus">
        <div class="contactuscontent">
            <div class="title">
                Contact

            </div>
            <div class="contactusform">
                <form action="{{ route('contactusguinea') }}" method="POST">
                    @csrf

                    <div class="row">
                        <div class="inputone">
                            <div class="firstname">Prénom <div class="star">*</div>
                            </div>

                            <input required name="firstname" type="text" placeholder="First Name">
                        </div>
                        <div class="inputtwo">
                            <div required class="firstname">Nom <div class="star">*</div>
                            </div>

                            <input required name="lastname" type="text" name="" id="" placeholder="Last Name">
                        </div>
                    </div>
                    <div class="row">

                        <div class="inputone">
                            Adresse e-mail

                            <input required name="emailaddress" type="email" placeholder="Email address">
                        </div>
                        <div class="inputtwo">
                            Pays
                            {{-- <input name="country" type="text" class="nigeria33" placeholder="guinea"> --}}
                            <select name="country" class="nigeria33" id=""><option value="">Guinea</option><option value="">Guinea</option></select>


                        </div>
                    </div>
                    <div class="row">
                        <div class="inputthree">
                            Message
                            <input required name="message" type="text" placeholder="Message">
                        </div>
                    </div>
                    <div class="submit">
                        <input required type="submit" name="" id="" value="Send Message">
                    </div>
                </form>
            </div>
        </div>
    </div>

    @component('footer.footer')
    @endcomponent
    <script>

    </script>
