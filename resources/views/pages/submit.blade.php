


@extends('layouts.mainlayout') @section('title')
<title> Lebanon Home Page </title>
@endsection
@extends('mainmenu.mainthree')
@section('content')
<div class="right">
    <div class="socialmedia">
        <div class="youtube">
            <i class="fa-solid fa-store"></i>
        </div>
        <div class="facebook">
            <i class="fa-brands fa-facebook-f"></i>
        </div>
      
        <div class="instagram">
            <i class="fa-brands fa-instagram"></i>
        </div>
    
    </div>
</div>
<div style="padding-top:120px">

    <div class="hometitlebanon contant">
        <div class="home"> Home/</div>
        <div class="nigeria">Nigeria/</div>
        <div class="service">Service Request/</div>
        <div class="real">In-home Service</div>

    </div>



    <div class="items3">
        @php
        $randomNumber = random_int(100000000, 999999999);
        @endphp
        <div class="title">Thank you for submitting your request!</div>
        <div class="text">Request Reference Number: {{ $randomNumber }}</div>

    </div>
    <div class="submitingg">
   
        <div class="card">
            <div class="address">
                <div class="addresscontent">
                    <div class="title">Service Centre Contact Details</div>
                 
                    <div class="location">
                        
                        <div class="loc-1"><i class="fa-solid fa-location-dot"></i></div>
                        <div class="text">22 Burma Road Apapa, Lagos State</div>
                    </div>
                    <div class="number">
                        <div class="phone"><i class="fa-solid fa-phone"></i></div>
                        <div class="phonetext">01 6334444 - 01 888 4444 </div>
                    </div>
                    <div class="email">
                        <div class="email-2"><i class="fa-solid fa-envelope"></i></div>
                        <div class="emailtext">support@fouani.com</div>
                    </div>
                </div>
            </div>

        </div>

    </div>
  
 

    
  
 




    {{-- <div class="submiting">
        <div> <i style="    position: relative;
                        left: 18px;
                        top: 2px; color:#e51240" class="fa-solid fa-chevron-left"></i> <input class="ps"
                type="submit" name="" id="" value="Previous Step"></div>
        <div><input class="ns" type="submit" name="" id="" value="Next Step"><i style="    position: relative;
                        right: 24px;
               
                        top: 2px;
                        color: white;" class="fa-solid fa-chevron-right"></i></div>
    </div> --}}

</div>

@component('footer.footer')
@endcomponent
@endsection


