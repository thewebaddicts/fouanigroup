@extends('layouts.mainlayout') @section('title')
    <title>
        Lebanon Home Page </title>
@endsection
@extends('mainmenu.mainthree')
@section('content')
    <div class="right">
        <div class="socialmedia">
            <div class="youtube">
                <i class="fa-solid fa-store"></i>
            </div>
            <div class="facebook">
                <i class="fa-brands fa-facebook-f"></i>
            </div>

            <div class="instagram">
                <i class="fa-brands fa-instagram"></i>
            </div>

        </div>
    </div>
    <div style="padding-top:188px">

        <div class="hometitlebanon contant">
            <div class="home"> Home/</div>
            <div class="nigeria">Nigeria/</div>
            <div class="service">Service Request/</div>
            <div class="real">In-home Service</div>

        </div>

        <div class="rednavbar5">
            <div class="allcircles">
                <div class="card">
                    <div class="circle">
                        1

                    </div>
                    <div class="text1">
                        Brand & Category
                    </div>
                </div>
                <div class="card">
                    <div class="circle-2">
                        2

                    </div>
                    <div class="text2">
                        Item Details
                    </div>
                </div>
                <div class="card">

                    <div class="circle-3">
                        3

                    </div>
                    <div class="text3">
                        Error Details
                    </div>
                </div>
                <div class="card">
                    <div class="circle-4">
                        4

                    </div>
                    <div class="text4">
                        Personal Information
                    </div>
                </div>

                <div class="card">
                    <div class="circle-5">
                        5

                    </div>
                    <div class="text5">
                        Request Summary
                    </div>
                </div>




            </div>

        </div>


        <div class="items2">
            <div class="title">Request Summary </div>
            <div class="text">Review your request before submitting</div>

        </div>
        <div class="summary">
            <div class="cards">
                <div class="card">
                    <div class="cardcontent">
                        <div class="t-circle">
                            <div class="c">1</div>
                            <div class="c2">Brand & Category</div>
                        </div>
                        <div class="midcont">
                            <div class="brand">






                                <div class="b-1">
                                    <div class="b-3">Brand</div>
                                    <div class="b-4"> @isset($brand->brand)
                                            {{ $brand->brand }}
                                        @endisset
                                    </div>
                                </div>
                                <div class="b-2">
                                    <div class="b-5">Category</div>
                                    <div class="b-6"> @isset($brand->category)
                                            {{ $brand->category }}
                                        @endisset
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="last">
                            <svg xmlns="http://www.w3.org/2000/svg" width="28.13" height="25" viewBox="0 0 28.13 25">
                                <path id="Icon_awesome-edit" data-name="Icon awesome-edit"
                                    d="M19.662,4.065l4.405,4.405a.478.478,0,0,1,0,.674L13.4,19.811l-4.532.5a.95.95,0,0,1-1.05-1.05l.5-4.532L18.988,4.065A.478.478,0,0,1,19.662,4.065Zm7.912-1.118L25.19.564a1.91,1.91,0,0,0-2.7,0L20.766,2.293a.478.478,0,0,0,0,.674l4.405,4.405a.478.478,0,0,0,.674,0l1.729-1.729a1.91,1.91,0,0,0,0-2.7ZM18.754,16.91v4.972H3.126V6.253H14.349a.6.6,0,0,0,.415-.171l1.954-1.954a.586.586,0,0,0-.415-1H2.344A2.345,2.345,0,0,0,0,5.472V22.663a2.345,2.345,0,0,0,2.344,2.344H19.535a2.345,2.345,0,0,0,2.344-2.344V14.956a.587.587,0,0,0-1-.415l-1.954,1.954A.6.6,0,0,0,18.754,16.91Z"
                                    transform="translate(0 -0.007)" fill="#67686a" />
                            </svg>

                        </div>
                    </div>
                </div>
                <div class="card2">
                    <div class="cardcontent">
                        <div class="t-circle">
                            <div class="c">2</div>
                            <div class="c2">Item Details</div>
                        </div>
                        <div class="midcont">
                            <div class="brand">
                                {{-- @foreach ($items as $item) --}}
                                <div class="b-2">
                                    <div class="b-5">Item Sub-category</div>
                                    <div class="b-6">
                                        @isset($items->item)
                                            {{ $items->item }}
                                        @endisset
                                    </div>
                                </div>
                                <div class="b-2">
                                    <div class="b-5">Size</div>
                                    <div class="b-6">
                                        @isset($items->size)
                                            {{ $items->size }}
                                        @endisset
                                    </div>
                                </div>
                                <div class="b-2">
                                    <div class="b-5">Model Number</div>
                                    <div class="b-6">
                                        @isset($items->model)
                                            {{ $items->model }}
                                        @endisset
                                    </div>
                                </div>
                                <div class="b-2">
                                    <div class="b-5">Serial Number</div>
                                    <div class="b-6">
                                        @isset($items->serialnumber)
                                            {{ $items->serialnumber }}
                                        @endisset
                                    </div>
                                </div>
                                <div class="b-1">
                                    <div class="b-3">Purchase Date</div>
                                    <div class="b-4">
                                        @isset($items->purchasedate)
                                            {{ $items->purchasedate }}
                                        @endisset
                                    </div>
                                </div>
                                {{-- @endforeach --}}
                                {{-- @foreach ($types as $type) --}}
                                <div class="b-2">
                                    <div class="b-5">Outdoor Unit Installation</div>
                                    <div class="b-6">
                                        @isset($types->type)
                                            {{ $types->type }}
                                        @endisset
                                    </div>
                                </div>
                                {{-- @endforeach --}}

                            </div>
                        </div>
                        <div class="last">
                            <svg xmlns="http://www.w3.org/2000/svg" width="28.13" height="25" viewBox="0 0 28.13 25">
                                <path id="Icon_awesome-edit" data-name="Icon awesome-edit"
                                    d="M19.662,4.065l4.405,4.405a.478.478,0,0,1,0,.674L13.4,19.811l-4.532.5a.95.95,0,0,1-1.05-1.05l.5-4.532L18.988,4.065A.478.478,0,0,1,19.662,4.065Zm7.912-1.118L25.19.564a1.91,1.91,0,0,0-2.7,0L20.766,2.293a.478.478,0,0,0,0,.674l4.405,4.405a.478.478,0,0,0,.674,0l1.729-1.729a1.91,1.91,0,0,0,0-2.7ZM18.754,16.91v4.972H3.126V6.253H14.349a.6.6,0,0,0,.415-.171l1.954-1.954a.586.586,0,0,0-.415-1H2.344A2.345,2.345,0,0,0,0,5.472V22.663a2.345,2.345,0,0,0,2.344,2.344H19.535a2.345,2.345,0,0,0,2.344-2.344V14.956a.587.587,0,0,0-1-.415l-1.954,1.954A.6.6,0,0,0,18.754,16.91Z"
                                    transform="translate(0 -0.007)" fill="#67686a" />
                            </svg>

                        </div>
                    </div>
                </div>
                <div class="card4">
                    <div class="cardcontent">
                        <div class="t-circle">
                            <div class="c">3</div>
                            <div class="c2">Error Details</div>
                        </div>
                        <div class="midcont">
                            <div class="brand">
                                <div class="b-1">
                                    {{-- @foreach ($error as $errors) --}}
                                    <div class="b-3">Error Type</div>
                                    <div class="b-4">
                                        @isset($error->error)
                                            {{ $error->error }}
                                        @endisset
                                    </div>
                                    {{-- @endforeach --}}
                                </div>
                                <div class="b-2">
                                    {{-- @foreach ($error as $errors) --}}
                                    <div class="b-5">Error Details</div>
                                    <div class="b-6">
                                        @isset($error->errortype)
                                            {{ $error->errortype }}
                                        @endisset
                                    </div>
                                    {{-- @endforeach --}}

                                </div>
                            </div>
                        </div>
                        <div class="last">
                            <svg xmlns="http://www.w3.org/2000/svg" width="28.13" height="25" viewBox="0 0 28.13 25">
                                <path id="Icon_awesome-edit" data-name="Icon awesome-edit"
                                    d="M19.662,4.065l4.405,4.405a.478.478,0,0,1,0,.674L13.4,19.811l-4.532.5a.95.95,0,0,1-1.05-1.05l.5-4.532L18.988,4.065A.478.478,0,0,1,19.662,4.065Zm7.912-1.118L25.19.564a1.91,1.91,0,0,0-2.7,0L20.766,2.293a.478.478,0,0,0,0,.674l4.405,4.405a.478.478,0,0,0,.674,0l1.729-1.729a1.91,1.91,0,0,0,0-2.7ZM18.754,16.91v4.972H3.126V6.253H14.349a.6.6,0,0,0,.415-.171l1.954-1.954a.586.586,0,0,0-.415-1H2.344A2.345,2.345,0,0,0,0,5.472V22.663a2.345,2.345,0,0,0,2.344,2.344H19.535a2.345,2.345,0,0,0,2.344-2.344V14.956a.587.587,0,0,0-1-.415l-1.954,1.954A.6.6,0,0,0,18.754,16.91Z"
                                    transform="translate(0 -0.007)" fill="#67686a" />
                            </svg>

                        </div>
                    </div>
                </div>
                <div class="card3">
                    <div class="cardcontent">
                        <div class="t-circle">
                            <div class="c">4</div>
                            <div class="c2">Personal Information</div>
                        </div>
                        <div class="midcont">
                            <div class="brand">
                                {{-- @foreach ($personal as $fn) --}}
                                <div class="b-1">
                                    <div class="b-3">First Name</div>
                                    <div class="b-4">
                                        @isset($personal->firstname)
                                            {{ $personal->firstname }}
                                        @endisset
                                    </div>
                                </div>
                                <div class="b-1">
                                    <div class="b-3">Last Name</div>
                                    <div class="b-4">
                                        @isset($personal->lastname)
                                            {{ $personal->lastname }}
                                        @endisset
                                    </div>
                                </div>
                                <div class="b-1">
                                    <div class="b-3">Email address</div>
                                    <div class="b-4">
                                        @isset($personal->emailaddress)
                                            {{ $personal->emailaddress }}
                                        @endisset
                                    </div>
                                </div>
                                <div class="b-1">
                                    <div class="b-3">Phone Number</div>
                                    <div class="b-4">
                                        @isset($personal->phone)
                                            {{ $personal->phone }}
                                        @endisset
                                    </div>
                                </div>
                                <div class="b-1">
                                    <div class="b-3">Region / State</div>
                                    <div class="b-4">
                                        @isset($personal->state)
                                            {{ $personal->state }}
                                        @endisset

                                    </div>
                                </div>
                                <div class="b-1">
                                    <div class="b-3">City</div>
                                    <div class="b-4">
                                        @isset($personal->city)
                                            {{ $personal->city }}
                                        @endisset
                                    </div>
                                </div>
                                <div class="b-1">
                                    <div class="b-3">Street Address </div>
                                    <div class="b-4">
                                        @isset($personal->street)
                                            {{ $personal->street }}
                                        @endisset
                                    </div>
                                </div>
                                <div class="b-1">
                                    <div class="b-3">Apartment, Building, Floor</div>
                                    <div class="b-4">
                                        @isset($personal->apartment)
                                            {{ $personal->apartment }}
                                        @endisset
                                    </div>
                                </div>
                                <div class="b-1">
                                    <div class="b-3">Appointment Date</div>
                                    <div class="b-4">
                                        @isset($personal->date)
                                            {{ $personal->date }}
                                        @endisset

                                    </div>
                                </div>
                                <div class="b-1">
                                    <div class="b-3">Appointment Time Slot</div>
                                    <div class="b-4">
                                        @isset($personal->time)
                                            {{ $personal->time }}
                                        @endisset
                                    </div>
                                </div>
                                {{-- @endforeach --}}

                            </div>
                        </div>
                        <div class="last">
                            <svg xmlns="http://www.w3.org/2000/svg" width="28.13" height="25" viewBox="0 0 28.13 25">
                                <path id="Icon_awesome-edit" data-name="Icon awesome-edit"
                                    d="M19.662,4.065l4.405,4.405a.478.478,0,0,1,0,.674L13.4,19.811l-4.532.5a.95.95,0,0,1-1.05-1.05l.5-4.532L18.988,4.065A.478.478,0,0,1,19.662,4.065Zm7.912-1.118L25.19.564a1.91,1.91,0,0,0-2.7,0L20.766,2.293a.478.478,0,0,0,0,.674l4.405,4.405a.478.478,0,0,0,.674,0l1.729-1.729a1.91,1.91,0,0,0,0-2.7ZM18.754,16.91v4.972H3.126V6.253H14.349a.6.6,0,0,0,.415-.171l1.954-1.954a.586.586,0,0,0-.415-1H2.344A2.345,2.345,0,0,0,0,5.472V22.663a2.345,2.345,0,0,0,2.344,2.344H19.535a2.345,2.345,0,0,0,2.344-2.344V14.956a.587.587,0,0,0-1-.415l-1.954,1.954A.6.6,0,0,0,18.754,16.91Z"
                                    transform="translate(0 -0.007)" fill="#67686a" />
                            </svg>

                        </div>
                    </div>
                </div>

            </div>
        </div>









        <div class="submiting">
            <div> <i style="    position: relative;
                                                                                                                            left: 18px;
                                                                                                                            top: 2px; color:#e51240"
                    class="fa-solid fa-chevron-left"></i>
                    <button  class="ps"
                    type="button" name="" id="" value="Previous Step" onclick='javascript:history.back()'>Previous Step</button>            </div>
            <a href="/submitrequest">
                <div><input class="ns" type="submit" name="" id="" value="Next Step"><i
                        style="    position: relative;
                                                                                                                            right: 24px;
                                                                                                                   
                                                                                                                            top: 2px;
                                                                                                                            color: white;" class="fa-solid fa-chevron-right"></i>
                </div>
            </a>
        </div>

    </div>

    @component('footer.footer')
    @endcomponent
@endsection
