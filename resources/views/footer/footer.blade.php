<div class="footer">
    <div class="footercontent">
        <div class="logo"><img src="/images/newlogo.svg" alt=""></div>
        <div class="footer2">
            <div class="address">
                <div class="addresscontent">
                    <div class="title">
                        Address
                    </div>
                    <div class="location">
                        <div class="loc-1"><i class="fa-solid fa-location-dot"></i></div>
                        <div class="text">City, Country
                            Floor, Building, Street</div>
                    </div>
                    <div class="number">
                        <div class="phone"><i class="fa-solid fa-phone"></i></div>
                        <div class="phonetext">01 6334444 - 01 888 4444 </div>
                    </div>
                    <div class="email">
                        <div class="email-2"><i class="fa-solid fa-envelope"></i></div>
                        <div class="emailtext">support@fouani.com</div>
                    </div>
                </div>
            </div>
            <div class="newsletter">
                <div class="title">
                    NewsLetter
                </div>
                <div class="newsletterinput">
                    <input class="news" type="text" name="" id="" placeholder="Email Address">
                    <input class="submit" type="submit" name="" id="" value="subscribe">
                </div>
                <div class="socialmedia">
                    <div class="youtube">
                        <i class="fa-brands fa-youtube"></i>
                    </div>
                    <div class="facebook">
                        <i class="fa-brands fa-facebook-f"></i>
                    </div>
                    <div class="twitter">
                        <i class="fa-brands fa-twitter"></i>
                    </div>
                    <div class="instagram">
                        <i class="fa-brands fa-instagram"></i>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <div class="footer3">
        <div class="footercontent3">
            <div class="termsandconditions">
                Terms & Conditions
            </div>
            <div class="privacyandpolicy">
                Privacy Policy
            </div>
            <div class="cookiespolicy">
                Cookies Policy
            </div>
           
          
        </div>
        <div class="developed">
            © {{ date('Y') }} Fouani Group. Designed and Developed by The Web Addicts
        </div>
    </div>
</div>
