<?php

$form_data = "<table>";
foreach($dictionary->form_data as $key=>$value ){
    $form_data .=  "<tr><td  style='width: 100px ; padding:5px 30px 5px 30px; font-size:12px ; font-weight:bolder ; text-transform: uppercase'>".  $key ."</td><td   style='padding:5px 30px 5px 30px; font-size:12px ; '> " . $value ."</td></tr>";
}
$form_data .= "</table>";
$email=$template->email_form;
$dictionary=[ "form_data"=>$form_data];
$Search = [];
$Replace = [];
foreach ($dictionary AS $key=>$value){
    $Search[]   =   "*".$key."*";
    $Replace[]  =   $value;
}

$email = str_replace( $Search, $Replace,$email );
?>

<div style="padding: 30px 0; font-size: 14px; border: 1px solid #eeeeee;display: flex;flex-direction: column ">
    <div style="margin:auto; max-width: 700px;  padding: 20px; color:#000;">{!! $email !!}</div>
    <div style="margin: 20px auto auto auto !important; max-width: 700px">
         <img   src="{{ env('APP_URL') }}/assets/images/Textlogo.png?v={{ env('CACHE_VERSION') }}" width="150">
    </div>
</div>