@php
$brand = \App\Models\guineelectricbrand::where('cancelled', 0)->get();

@endphp
<nav>
    <div class="navbar-2 contant">
        <div class="logo">
            <a href="/"> <img src="/images/newlogo.svg" alt=""> </a>
        </div>
        <div class="slide-select" style="position: relative;
        right: 200px;">
            <div class="select-wrapper">
                <div class="select">
                    <div class="select__trigger" style="
                "> <span style="display:flex;justify-content:center;algin-items:center;column-gap: 10px;
"> <img src="/images/guinea.png"
                                alt=""> Guinea</span>
                        <div class="arrow">

                        </div>
                    </div>
                    <div class="custom-options">
                        <div class="select__trigger-2" style="    position: relative;
                        left: -7px;"> <img src="/images/Guinea.png" alt=""> Guinea
                            <div class="arrow"></div>
                        </div>

                        <a href="/liberia"><img src="/images/liberia.png" alt=""> Liberia</a>
                        <a href="/congohome"><img src="/images/congos.png" alt=""> Congo</a>
                        <a href="/nigeriahome"> <img src="/images/nigeria.png" alt=""> Nigeria</a>
                        <a href="/gambia"><img src="/images/gambia-flag.png" alt=""> Gambia</a>
                        <a href="/lebanon"><img src="/images/lebanon.png" alt=""> Lebanon</a>


                    </div>
                </div>
            </div>




        </div>
        <div class="navbarcontent">
            <ul>
             
                    <li>Électronique <i class="fa-solid fa-angle-down"></i>
                        <div class="dropdown">

                            <div class="dropdown-content">
                                <div class="dp1">Marques</div>
                                @foreach ($brand as $brands)
                                 <a href="/guineaelectronicbrands/{{ $brands->id }}">   <div class="dp">{{ $brands->brandlabel }}</div> </a>
                                @endforeach


                            </div>
                        </div>
                    </li>
                
                <a href="">
                    <li>Service Technique</li>
                </a>
                <a href="/guineaimmobilier">
                    <li>Immobilier</li>
                </a>
                <a href="">
                    <li>Brands</li>
                </a>
                <a href="/contactus">
                    <li>Contact</li>
                </a>
                <a href="/careers">
                    <li>Carrières</li>
                </a>



            </ul>
        </div>
    </div>

</nav>
<script>
    document.querySelector('.select-wrapper').addEventListener('click', function() {
        this.querySelector('.select').classList.toggle('open');
    })
</script>
