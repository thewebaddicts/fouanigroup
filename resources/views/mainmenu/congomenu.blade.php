@php
$brands = \App\Models\liberiaelectricbrand::where('cancelled', 0)->get();

@endphp

<nav>
    <div class="navbar-2 contant">
        <div class="logo">
            <a href="/"> <img src="/images/newlogo.svg" alt=""> </a>
        </div>
        <div class="slide-select-2" style="    display: flex;
        justify-content: center;
        align-items: center;">
            <div class="select-wrapper">
                <div class="select">
                    <div class="select__trigger" style="position: relative; right:50px"> <span
                            style="display:flex;justify-content:center;algin-items:center;column-gap: 10px;
                            "> <img
                                src="/images/congos.png" alt=""> Congo</span>
                        <div class="arrow">

                        </div>
                    </div>
                    <div class="custom-options" style="left: -46px;
                    right: 0px;">
                        <div class="select__trigger-2" style="width:99px"> <img src="/images/congos.png" alt="">
                            Congo
                            <div class="arrow"></div>
                        </div>

                        <a href="/nigeriahome"><img src="/images/nigeria.png" alt=""> Nigeria</a>
                        <a href="/congohome"><img src="/images/congos.png" alt=""> Congo</a>
                        <a href="/guinea"> <img src="/images/guinea.png" alt=""> Guinea</a>
                        <a href="/gambia"><img src="/images/gambia-flag.png" alt=""> Gambia</a>
                        <a href="/lebanon"><img src="/images/lebanon.png" alt=""> Lebanon</a>


                    </div>
                </div>
            </div>




        </div>
        <div class="navbarcontent">
            <ul style="column-gap:12px">
                <a href="">
                    <li>Salles D’exposition</li>
                </a>


             
                    <li>Électronique <i class="fa-solid fa-angle-down"></i>
                        <div class="dropdown">

                            <div class="dropdown-content">
                                <div class="dp1">Marques</div>
                                @foreach ($brands as $brand)
                                 <a href="/congoelectronicbrands/{{ $brand->id }}">   <div class="dp">{{ $brand->brandname }}</div> </a>
                                @endforeach

                            </div>
                        </div>
                    </li>
        

                <a href="">
                    <li>Service Technique</li>
                </a>

                <a href="/congomeuble">
                    <li>Meubles</li>
                </a>
                <a href="">
                    <li>Brands</li>
                </a>
                <a href="">
                    <li>Département Des Ventes</li>
                </a>
                <a href="/contactus">
                    <li>Contact</li>
                </a>
                <a href="/careers">
                    <li>Carrières</li>
                </a>


            </ul>
            <a class="magasin" href="">Magasin En Ligne</a>
        </div>
    </div>

</nav>
<script>
    document.querySelector('.select-wrapper').addEventListener('click', function() {
        this.querySelector('.select').classList.toggle('open');
    })
</script>
