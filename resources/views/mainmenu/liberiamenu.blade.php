@php
$brands = \App\Models\liberiaelectricbrand::where('cancelled', 0)->get();
$fmcg=\App\Models\liberianfmcg::where('cancelled',0)->get();


@endphp
<nav>
    <div class="navbar-3 contant">
        <div class="logo">
            <a href="/"> <img src="/images/newlogo.svg" alt=""> </a>
        </div>
        <div class="slide-select-2">
            <div class="select-wrapper">
                <div class="select">
                    <div class="select__trigger"> <span style="display:flex;justify-content:center;algin-items:center;column-gap: 10px;
                        ">
                            <img src="/images/liberia.png" alt=""> Liberia</span>
                        <div class="arrow">

                        </div>
                    </div>
                    <div class="custom-options">
                        <div class="select__trigger-2"> <img src="/images/liberia.png" alt="">
                            Liberia
                            <div class="arrow"></div>
                        </div>

                        <a href="/nigeriahome"><img src="/images/nigeria.png" alt=""> Nigeria</a>
                        <a href="/congohome"><img src="/images/congos.png" alt=""> Congo</a>
                        <a href="/guinea"> <img src="/images/guinea.png" alt=""> Guinea</a>
                        <a href="/gambia"><img src="/images/gambia-flag.png" alt=""> Gambia</a>
                        <a href="/lebanon"><img src="/images/lebanon.png" alt=""> Lebanon</a>


                    </div>
                </div>
            </div>




        </div>
        <div class="navbarcontent">
            <ul>
               
                    <li>Electronics <i class="fa-solid fa-angle-down"></i>
                        <div class="dropdown">

                            <div class="dropdown-content">
                                <div class="dp1">Brands</div>
                                @foreach ($brands as $brand)
                                  <a style="color: black" href="/liberiabrands/{{ $brand->id }}">  <div class="dp">{{ $brand->brandname }}</div></a>
                                   
                                @endforeach

                            </div>
                        </div>
                    </li>
           


              
                    <li>FMCG
                        <i class="fa-solid fa-angle-down"></i>
                        <div class="dropdown">

                            <div class="dropdown-content">
                             <a href="/fmcgliberia" >  <div class="dp1">Brands</div> </a> 
                                @foreach ( $fmcg as $droco )
                                    
                        
                              <a style="color: black" href="/liberianfmcg/{{ $droco->id }}">  <div class="dp">{{ $droco->label }}</div></a>
                                @endforeach
                           

                            </div>
                        </div>
                    </li>
             

                <a href="#">
                    <li>Packaging</li>
                </a>

                <a href="/contactus">
                    <li>Contact us</li>
                </a>
                <a href="/careers">
                    <li>Careers</li>
                </a>

            </ul>
        </div>
    </div>

</nav>
<script>
    document.querySelector('.select-wrapper').addEventListener('click', function() {
        this.querySelector('.select').classList.toggle('open');
    })
</script>
