@php
$brands = \App\Models\nigeribrands::where('cancelled', 0)->get();
$outlits = \App\Models\nigerioutlits::where('cancelled', 0)->get();
$fmcg = \App\Models\fmcg::where('cancelled', 0)->get();

@endphp

<nav>
    <div class="navbar-4 contant">
        <div class="logo">
            <a href="/"> <img src="/images/newlogo.svg" alt=""> </a>
        </div>
        <div class="slide-select">
            <div class="select-wrapper">
                <div class="select">
                    <div class="select__trigger"> <span style="display:flex;justify-content:center;algin-items:center;column-gap: 10px;
                        ">
                            <img src="/images/nigeria.png" alt=""> Nigeria</span>
                        <div class="arrow">

                        </div>
                    </div>
                    <div class="custom-options">
                        <div class="select__trigger-2"> <img src="/images/nigeria.png" alt="">
                            Nigeria
                            <div class="arrow"></div>
                        </div>

                        <a href="/liberia"><img src="/images/liberia.png" alt=""> Liberia</a>
                        <a href="/congohome"><img src="/images/congos.png" alt=""> Congo</a>
                        <a href="/guinea"> <img src="/images/guinea.png" alt=""> Guinea</a>
                        <a href="/gambia"><img src="/images/gambia-flag.png" alt=""> Gambia</a>
                        <a href="/lebanon"><img src="/images/lebanon.png" alt=""> Lebanon</a>


                    </div>
                </div>
            </div>




        </div>
        <div class="navbarcontent">
            <ul>
             
                    <li>Electronics <i class="fa-solid fa-angle-down"></i>
                        <div class="dropdown">

                            <div class="dropdown-content">
                                <div class="dp1">Brands</div>
                                @foreach ($brands as $brand)
                                    <a style="color: black" class="lg1" href="/brand/{{ $brand->id }}">
                                        <div class="dp">{{ $brand->nigeriahomelabel }}</div>
                                    </a>
                                @endforeach

                            </div>
                        </div>



                    </li>

                
             
                    <li>Outlets
                        <i class="fa-solid fa-angle-down"></i>
                        <div class="dropdown">

                            <div class="dropdown-content">

                                @foreach ($outlits as $card)
                                    <a style="color: black" href="/outlet/{{ $card->id }}">
                                        <div class="dp">{{ $card->label }}</div>
                                    </a>
                                @endforeach
                            </div>
                        </div>

                    </li>
              
            
                    <li>Service Request
                        <i class="fa-solid fa-angle-down"></i>
                        <div class="dropdown">

                            <div class="dropdown-content">

                              <a style="color: black" href="/installation">  <div class="dp">Installation Request</div> </a>
                             <a style="color: black" href="/categories">   <div class="dp">In-home Service</div> </a>

                            </div>
                        </div>
                    </li>
          
              
                    <li>FMCG
                        <i class="fa-solid fa-angle-down"></i>
                        <div class="dropdown">

                            <div class="dropdown-content">
                                <div class="dp1">Brands</div>
                                @foreach ($fmcg as $dp)
                                <a style="color: black" href="/fmcg/{{ $dp->id }}">
                                    <div class="dp">{{ $dp->brandname }}</div>
                                </a>
                            @endforeach

                            </div>
                        </div>
                    </li>
          
                <a href="/manufacturing">
                    <li>Manufacturing</li>
                </a>
                <a href="/contactus">
                    <li>Contact us</li>
                </a>

                <a href="/careers">
                    <li>Careers</li>
                </a>


            </ul>
            <a class="onlinestore" href="">
                Online Store

            </a>
        </div>
    </div>

</nav>
<script>
    document.querySelector('.select-wrapper').addEventListener('click', function() {
        this.querySelector('.select').classList.toggle('open');
    })
</script>
