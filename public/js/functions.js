function ShowLoader() {
    $("body").append(
        '<div class="common-loader-initial common-loader-noanim"></div>'
    );
}
function HideLoader() {
    $(".common-loader-initial").remove();
}

function ShowMessage(Title, Text) {
    $.fancybox.open(
        '<div class="message"><h2>' + Title + "</h2><p>" + Text + "</p></div>"
    );
}

function ShowIframe(link, maxwidth) {
    $.fancybox.open({
        src: link,
        type: "iframe",
        opts: {
            iframe: {
                css: {
                    width: maxwidth,
                },
            },
        },
    });
}
// function regex($url,$matches){
//     preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $url, $matches);

// }

function removeQueryNotificationString() {

    urlObject = new URL(window.location.href);
    urlObject.searchParams.delete('notification');
    urlObject.searchParams.delete('notification_message');
    urlObject.searchParams.delete('notification_title');

    url = urlObject.href;

    history.pushState(null, null, url);

}

function closeFancyBox() {
    parent.jQuery.fancybox.getInstance().close();
}

var isMobile = false; //initiate as false
// device detection
if (
    /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(
        navigator.userAgent
    ) ||
    /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
        navigator.userAgent.substr(0, 4)
    )
) {
    isMobile = true;
}

function doParallax() {
    $(".parallax, .parallaxText").css({
        transition: "0s !important",
        "transition-delay": "0s !important",
        "transition-timing-function": "linear",
    });
    if (isMobile != true) {
        $(".parallax").each(function () {
            var offset = $(this).offset();
            var positionY = (window.pageYOffset - offset.top) / 2;
            if (positionY < 0) {
                positionY = 0;
            }
            if (positionY < 100) {
                $(this)
                    .find(".parallaxText")
                    .css("opacity", "" + (100 - positionY) / 100);
            }
            $(this).css("background-position", "center +" + positionY + "px");
        });
    }
}
function search_animal() {
    // alert('hey');
    let input = document.getElementById('search').value
    input = input.toLowerCase();
    let x = document.getElementsByClassName('animals');

    for (i = 0; i < x.length; i++) {
        if (!x[i].innerHTML.toLowerCase().includes(input)) {
            x[i].style.display = "none";
        } else {
            x[i].style.display = "list-item";
        }
    }
}


window.addEventListener("scroll", reveal);

function scrollup() {
    document.querySelectorAll('a[href^="#"]').forEach(anchor => {
        anchor.addEventListener('click', function (e) {
            e.preventDefault();

            document.querySelector(this.getAttribute('href')).scrollIntoView({
                behavior: 'smooth'


            });
        });
    });

}












// $( "li.item-ii" ).find( "li" ).css( "background-color", "red" );






















function InitializeMenuScroll() {
    var scroll_start = 0;
    var startchange = $(".menu-marker");
    var offset = startchange.offset();
    //  var slideshowOffset = $("#slideshow").offset();
    if (startchange.length >= 0 && $(document).scrollTop() - offset.top >= 0) {
        $(".main-nav-2").addClass("display");
        $(".main-nav").hide();
    } else {
        $(".main-nav-2").removeClass("display");
        $(".main-nav").show();
    }
}

function InitializeMenuMobile() {
    $("nav.mobile-nav .burger").click(function () {
        $(this).toggleClass("clicked");
        $(this).parent().find("ul").slideToggle(200);
    });
  
}
function homepageaboutus(){
    $('.title-nav-1').click(function(){
        $('.title-nav-1').removeClass('active');
        $(this).toggleClass('active');
    })

}




function openAccordion(elem) {
    elem.toggleClass("accordion-active");

    var elemMarginBtm = elem.css("margin-bottom");
    console.log(elemMarginBtm);

    var panel = elem.next();
    if (panel.css("max-height") != "0px") {
        panel.css("max-height", "0px");
    } else {
        panel.css("max-height", panel.prop("scrollHeight"));
    }
}

function overview(overviewid){
  
    $('.aboutusdetails').hide();
    $('.aboutusdetails_2').hide();

    $('.first').hide();
  





    

    
  
   

    $('#' + overviewid).show()
  
}
function showmore(){
    $('.showmore').click(function(){
        $('.mainbrands').css('display','none');
        $('.mainbrands2').css('display','flex');
    
    })


}

function categories(brandid){
 
    $('.aboutusdetails').hide();
    $('.aboutusdetails_3').hide();


    $('#' + brandid).show();
}
function prod(brandid){
 
    $('.aboutusdetails_3').show();

    $('#' + brandid).show();
}

// function gettingvalue(){

//         $('.thisinput').click(function() {
//           $('.thisinput').val();
          
//         });
   

// }
function gettingvalues(){

    $('.brands2').click(function() {
      $('#thisinput2').val();
      
    });


}
function gettingvaluess(){

    $('.brands3').click(function() {
      $('#thisinput3').val();
      
    });


}

function classactive(){
    $('.brand4').click(function() {
       
        $('.brand4').removeClass('active');
        $(this).toggleClass('active');
       
      
        
        
      });

}
function activeclass(){
    $('.brand5').click(function() {
       
    
        $(this).toggleClass('active');
       
      
        
        
      });
    
}
function type(){
    $('.brand5').click(function(){
        $('.brand5').removeClass('active');
        $(this).toggleClass('active')
    })
}

function types(){
    $('.brand4').click(function () {    
        $('.thisinput').prop('checked', false);
        var val =  $(this).find('input:radio').prop('checked')?false:true;
        $(this).find('input:radio').prop('checked', val);
        checkedValue = $(this).find('input:radio').val();
    
    });
}
function brands(){
    $('.brand5').click(function () {    
        $('.thisinputs').prop('checked', false);
        var val =  $(this).find('input:radio').prop('checked')?false:true;
        $(this).find('input:radio').prop('checked', val);
        checkedValue = $(this).find('input:radio').val();
    
    });

}
function theprod(overviewid){
    $('.aboutusdetails_2').show()


    $('#' + overviewid).show()
}

function fromplustominus(){
    $('.list_1').click(function () {    
        $('.plus').removeClass('active');
        $('.plus').toggleClass('active');
   
    
    
    });

}

















function AccordionInitiate() {

    $('.accordion .accordion-item:not(.active) .accordion-content').addClass('initialized');
    $('.accordion .accordion-item:not(.active) .accordion-content').slideUp(0);
    $('.accordion .accordion-title').click(function () {
        $(this).parents('.accordion').find('.accordion-item.active').not($(this).parents('.accordion-item')).removeClass('active');
        $(this).parents('.accordion-item').toggleClass('active');
        $(this).parents('.accordion-item').find('.accordion-content').slideToggle(200);

        // try {
        //     bLazy.revalidate();
        // }catch (e) {

        // }
        $(this).parents('.accordion').find('.accordion-item:not(.active)').find('.accordion-content').slideUp(200);
    });

}
function showroom(showroomid){
    $('.aboutusdetails').hide();
    $('.aboutusdetails_4').hide();


    $('#' + showroomid).show();

}
function theshowroom(showroomid){
    $('.aboutusdetails_4').show();

    $('#' + showroomid).show();


}
function searching3() {
    jQuery("#inputString").keyup(function () {
   

        var filter = jQuery(this).val();
        var count = 0
        
            

            jQuery(".list").each(function () {
                if (jQuery(this).text().search(new RegExp(filter, "i")) < 0) {
                    jQuery(this).hide();
                } else {
                   
    
                    jQuery(this).show();
    
                }
            });

        

       
    });
}