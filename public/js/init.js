var version = document.currentScript.getAttribute("attr-cache-version");

function loadCss(url) {
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = url;
    document.getElementsByTagName("head")[0].appendChild(link);
}

loadCss("/css/styles.css?v=" + version);

loadCss("https://use.fontawesome.com/releases/v5.7.1/css/all.css");
loadCss("/js/owl.carousel/dist/assets/owl.carousel.min.css");
loadCss("/js/jquery.fancybox/dist/jquery.fancybox.min.css");
loadCss("https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css");
loadCss(
    "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
);


requirejs.config({
    waitSeconds: 200,
    paths: {
        functions: "/js/functions.js?v=" + version,
        jquery: "/js/jquery/dist/jquery.min.js?v=" + version,
        owl: "/js/owl.carousel/dist/owl.carousel.min.js?v=" + version,
        fancybox:
            "/js/jquery.fancybox/dist/jquery.fancybox.min.js?v=" + version,
        aos: "https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos",
        select2 :"https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min"
    },
    shim: {
        functions: {
            deps: ["jquery", "fancybox"],
        },
        owl: {
            deps: ["jquery"],
        },
        aos: {
            deps: ["jquery"],
        },
        blazy: {
            deps: ["jquery"],
        },
        fancybox: {
            deps: ["jquery"],
        },
        select2: {
            deps: ["jquery"],
        },
    },
});

//Define dependencies and pass a callback when dependencies have been loaded
require(["jquery"], function ($) {
    jQuery.event.special.touchstart = {
        setup: function (_, ns, handle) {
            this.addEventListener("touchstart", handle, { passive: true });
        },
    };
    var is_safari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    if (!is_safari) {
        $(window).on("beforeunload", function () {
            ShowLoader();
        });
    }

});


require(["functions"], function () {
    window.addEventListener("scroll", function () {
        InitializeMenuScroll();
    });

    try {
        InitializeMenuScroll();
    } catch (e) {
        console.log(e);
    }

    try {
        InitializeMenuMobile();
    } catch (e) {
        console.log(e);
    }

    try {
        console.log("before");
        FooterFunctions();
        console.log("after");
    } catch (e) {
        console.log(e);
    }

    try {
        FooterFunctions1();
    } catch (e) {
        console.log(e);
    }
    try {
        NotificationFunction();
    } catch (e) {}


    try{
        homepageaboutus()
    }
    catch{
        
    }

    try{
        overview()
    }
    catch{

    }
    try{
        showmore()

    }
    catch{

    }

    try{
        categories()

    }
    catch{

    }
    try{
        gettingvalue()
    }
    catch{

    }
    try{
        gettingvalues()
    }
    catch{

    }
    try{
        gettingvaluess()

    }
    catch{

    }
    try{
        classactive()
    }
    catch{

    }

    try{
        activeclass()

    }
    catch{

    }
    try{
        type();
    }
    catch{

    }

    try{
        types();
    }
    catch{

    }
    try{
        brands();
    }
    catch{

    }
    try{
        theprod();

    }
    catch{

    }
    try{
        prod();
    }
    catch{
        
    }
    try{
        showroom()

    }
    catch{

    }
    try{
        theshowroom()

    }
    catch{

    }
    try{
        searching3();

    }
    catch{

    }
    try{
        AccordionInitiate();
    }
    catch{
        
    }
 
 
 
    
 
    
  
    
 
    
  
    
  
    
 
    
  
    
  
    
   
  
 

    
   
 
  
  
 
 
  
  
  
  

  
  

  

 

   

    






















  













   
   
  
   
    
  
  
   
   
  
    
    
   
    
    
});

require(["aos"], function (AOS) {
    AOS.init({easing: "ease-in-out-sine", duration: 600, once: false, mirror: true});
});

require(["select2"], function (AOS) {
    $('.js-example-basic-single').select2({
        minimumResultsForSearch: -1
    });
  
    

});


// require(["select2"],function(){
    
//         $('.js-example-basic-multiple').select2();

// })


require(["owl"], function (owlCarousel) {
    $(".carousel").each(function (index, elem) {
        var owl = $(elem);
        owl.owlCarousel({
            items: parseInt($(elem).attr("data-carousel-items")),
            nav: $(elem).attr("data-carousel-nav") === "true" ? true : false,
            dots: $(elem).attr("data-carousel-dots") === "true" ? true : false,
            autoplay:
                $(elem).attr("data-carousel-autoplay") === "true"
                    ? true
                    : false,
            slideTransition: "linear",
            autoplayHoverPause: false,
            autoWidth:
                $(elem).attr("data-carousel-autowidth") === "true"
                    ? true
                    : false,
            rtl: false,
            autoHeight: true,
        

            // animateOut: $(elem).attr("data-carousel-animate"),
            loop: $(elem).attr("data-carousel-loop") === "true" ? true : false,
            navText: [
                "<img src='/images/arrowL.png' alt=''>",
                "<img src='/images/arrowR.png' alt=''>"
            ],
        });
        $(".owl-dot").click(function () {
            owl.trigger("to.owl.carousel", [$(this).index(), 300]);
        });
        // owl.on("changed.owl.carousel", function(event) {
        //     bLazy.revalidate();
        // });
        // owl.on("initialized.owl.carousel", function(event) {
        //     bLazy.revalidate();
        // });
        // bLazy.revalidate();
    });

    if ($(".single-carousel").length > 0) {
        var owl = $(".single-carousel");
        owl.owlCarousel({
            items: 1,
            nav: false,
            dots: true,
            //dotsContainer: ".owl-dots-custom",
            // autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: false,
            rtl: false,
            loop: true,
            rewind: true,
        });
        // owl.on("changed.owl.carousel", function(event) {
        //     bLazy.revalidate();
        // });
        // owl.on("initialized.owl.carousel", function(event) {
        //     bLazy.revalidate();
        // });
    }

    if ($(".multi-carousel").length > 0) {
        var owl2 = $(".multi-carousel");
        owl2.owlCarousel({
            autoWidth: true,
            autoplay: false,
            //rtl: true,
            loop: false,
            nav: false,
            // items: 7,
            margin: 20,
            // padding: 100,
            dots: false,
            rewind: true,
        });
        // owl2.on("changed.owl.carousel", function (event) {
        //     bLazy.revalidate();
        // });
        // owl2.on("initialized.owl.carousel", function (event) {
        //     bLazy.revalidate();
        // });
    }
});

require(["fancybox"], function () {
    $.fancyConfirm = function (opts) {
        opts = $.extend(
            true,
            {
                title: "Are you sure?",
                message: "",
                okButton: "OK",
                noButton: "Cancel",
                callback: $.noop,
            },
            opts || {}
        );

        $.fancybox.open({
            type: "html",
            autoDimensions: true,
            src:
                '<div class="fc-content">' +
                "<h3>" +
                opts.title +
                "</h3>" +
                "<p>" +
                opts.message +
                "</p>" +
                '<div class="text-right confirmation">' +
                '<a data-value="0" data-fancybox-close>' +
                opts.noButton +
                "</a>" +
                '<button data-value="1" data-fancybox-close class="rounded">' +
                opts.okButton +
                "</button>" +
                "</div>" +
                "</div>",
            opts: {
                animationDuration: 350,
                animationEffect: "material",
                modal: true,
                baseTpl:
                    '<div class="fancybox-container fc-container" role="dialog" tabindex="-1">' +
                    '<div class="fancybox-bg"></div>' +
                    '<div class="fancybox-inner">' +
                    '<div class="fancybox-stage"></div>' +
                    "</div>" +
                    "</div>",
                afterClose: function (instance, current, e) {
                    var button = e ? e.target || e.currentTarget : null;
                    var value = button ? $(button).data("value") : 0;

                    opts.callback(value);
                },
            },
        });
    };

  
});
